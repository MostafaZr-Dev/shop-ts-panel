import { useCallback, useState } from 'react'
import httpService from 'services/httpService'

type PostAPIParams = {
  url: string;
  configs?: object;
}

type PostAPIResponse = {
  data: any;
  isLoading: boolean;
  success: boolean;
  error: boolean;
}

// eslint-disable-next-line no-unused-vars
const usePostAPI = ({ url, configs = {} }: PostAPIParams): [PostAPIResponse, <T, B>(data: B) => void] => {
  const [res, setRes] = useState<PostAPIResponse>({
    data: null,
    isLoading: false,
    success: false,
    error: false
  })

  const callPostAPI = useCallback(
    <T, B>(data: B) => {
      setRes((prevState) => ({
        ...prevState,
        isLoading: true
      }))

      httpService
        .post<T, B>(url, data, configs)
        .then((response) => {
          setRes({
            isLoading: false,
            success: true,
            error: false,
            data: response.data
          })
        })
        .catch((error) => {
          setRes({
            isLoading: false,
            success: false,
            error: true,
            data: null
          })
          console.log(error)
        })
    },
    [url, configs]
  )

  return [res, callPostAPI]
}

export default usePostAPI
