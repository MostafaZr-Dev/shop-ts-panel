// eslint-disable-next-line no-use-before-define
import React from 'react'

import { RouteWithSubRoutes } from '../../router'
import Layout from './components/layout'

type PanelProps = {
  base: string;
  routes: []
}

function Panel ({ base, routes }: PanelProps) {
  const renderRoutes = routes.map((route, index) => (
    <RouteWithSubRoutes key={`${base}-${index}`} base={base} route={route} />
  ))

  return (
    <Layout>
      {renderRoutes}
    </Layout>
  )
}

export default Panel
