// eslint-disable-next-line no-use-before-define
import React, { useState } from 'react'

import { Grid } from '@material-ui/core'

import Sidebar from '../sidebar'
import Content from '../content'

function Layout (props: React.PropsWithChildren<{}>) {
  const [isSidebarOpen, setIsSidebarOpen] = useState(false)

  const toggleDrawer = () => {
    setIsSidebarOpen((prevState) => !prevState)
  }

  const closeDrawer = () => {
    setIsSidebarOpen(false)
  }

  return (
    <Grid container>
      <Sidebar open={isSidebarOpen} closeDrawer={closeDrawer} />
      <Content toggleDrawer={toggleDrawer}>{props.children}</Content>
    </Grid>
  )
}

export default Layout
