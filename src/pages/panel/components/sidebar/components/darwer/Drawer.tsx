// eslint-disable-next-line no-use-before-define
import React from 'react'
import { DrawerWrapper } from './DrawerStyles'

type SidebarDrawerProps = {
  open: boolean;
  onClose: () => void;
  children: React.ReactNode
}

function SidebarDrawer ({ open, onClose, children }:SidebarDrawerProps) {
  return (
    <DrawerWrapper anchor="left" open={open} onClose={onClose}>
      {children}
    </DrawerWrapper>
  )
}

export default SidebarDrawer
