// eslint-disable-next-line no-use-before-define
import React from 'react'
import { ListItemText } from '@material-ui/core'
import { useTheme } from '@material-ui/core/styles'

import { StyledSubItemContainer, StyledBadge, StyledListItemIcon } from './MenuStyles'

type SubItemProps = {
    title: string;
    icon: React.ReactNode;
    badge?: boolean | undefined;
    badgeValue?: number;
}

function SubItem ({ title, icon, badge, badgeValue }:SubItemProps) {
  const theme = useTheme()

  return (
    <StyledSubItemContainer button>
      <StyledListItemIcon open={false} theme={theme} className="sub-item">{icon}</StyledListItemIcon>
      <ListItemText primary={title} />
      {badge && <StyledBadge badgeContent={badgeValue} />}
    </StyledSubItemContainer>
  )
}

export default SubItem
