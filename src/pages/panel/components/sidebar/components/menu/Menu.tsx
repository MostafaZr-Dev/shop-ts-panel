/* eslint-disable no-use-before-define */
import React, { useState } from 'react'
import {
  AddBox,
  Category,
  ListAlt,
  Storefront,
  ShoppingBasket,
  CardGiftcard,
  Payment,
  Money,
  MoneyOff,
  Settings
} from '@material-ui/icons'
import { useLocation } from 'react-router-dom'

import { Wrapper, SubMenuWrapper, StyledRouteLink } from './MenuStyles'
import MenuItem from './MenuItem'
import SubItem from './SubItem'

function Menu () {
  const { pathname } = useLocation()

  const [current, setCurrent] = useState(pathname)

  const handleChange = (index: string) => {
    setCurrent(index)
  }

  return (
    <Wrapper aria-labelledby="nested-list-subheader">
      <MenuItem
        icon={<Storefront />}
        title="محصولات"
        open={current.includes('products')}
        onChange={(e) => {
          handleChange('/dashboard/products')
        }}
      >
        <SubMenuWrapper>
          <StyledRouteLink
            exact
            to="/dashboard/products/create"
            activeClassName="active-item"
          >
            <SubItem title="محصول جدید" icon={<AddBox />} />
          </StyledRouteLink>
          <StyledRouteLink
            exact
            to="/dashboard/products"
            activeClassName="active-item"
          >
            <SubItem title="لیست محصولات" icon={<ListAlt />} />
          </StyledRouteLink>
          <StyledRouteLink
            exact
            to="/dashboard/products/offers"
            activeClassName="active-item"
          >
            <SubItem title="پیشنهادهای ویژه" icon={<CardGiftcard />} />
          </StyledRouteLink>
        </SubMenuWrapper>
      </MenuItem>
      <MenuItem
        icon={<Category />}
        title="دسته بندی ها"
        open={current.includes('categories')}
        onChange={(e) => {
          handleChange('/dashboard/categories')
        }}
      >
        <SubMenuWrapper>
          <StyledRouteLink
            exact
            to="/dashboard/categories/create"
            activeClassName="active-item"
          >
            <SubItem title="ایجاد دسته بندی" icon={<AddBox />} />
          </StyledRouteLink>
          <StyledRouteLink
            exact
            to="/dashboard/categories"
            activeClassName="active-item"
          >
            <SubItem title="لیست دسته بندی ها" icon={<ListAlt />} />
          </StyledRouteLink>
        </SubMenuWrapper>
      </MenuItem>
      <MenuItem
        icon={<ShoppingBasket />}
        title="مدیریت سفارشات"
        open={current.includes('orders')}
        onChange={(e) => {
          handleChange('/dashboard/orders')
        }}
      >
        <SubMenuWrapper>
          <StyledRouteLink
            exact
            to="/dashboard/orders"
            activeClassName="active-item"
          >
            <SubItem title="لیست سفارشات" icon={<ListAlt />} badge badgeValue={89} />
          </StyledRouteLink>
        </SubMenuWrapper>
      </MenuItem>
      <MenuItem
        icon={<Money />}
        title="مدیریت مالی"
        open={current.includes('financial')}
        onChange={(e) => {
          handleChange('/dashboard/financial')
        }}
      >
        <SubMenuWrapper>
          <StyledRouteLink
            exact
            to="/dashboard/financial/payments"
            activeClassName="active-item"
          >
            <SubItem title="پرداخت ها" icon={<Payment />} />
          </StyledRouteLink>
          <StyledRouteLink
            exact
            to="/dashboard/financial/coupons"
            activeClassName="active-item"
          >
            <SubItem title="کدهای تخفیف" icon={<MoneyOff />} />
          </StyledRouteLink>
        </SubMenuWrapper>
      </MenuItem>
      <MenuItem
        icon={<Settings />}
        title="تنظیمات"
        open={current.includes('settings')}
        onChange={(e) => {
          handleChange('/dashboard/settings')
        }}
      >
        <SubMenuWrapper>
          <StyledRouteLink
            exact
            to="/dashboard/settings"
            activeClassName="active-item"
          >
            <SubItem title="لیست تنظیمات" icon={<ListAlt />} />
          </StyledRouteLink>
        </SubMenuWrapper>
      </MenuItem>
    </Wrapper>
  )
}

export default Menu
