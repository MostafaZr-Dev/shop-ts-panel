// eslint-disable-next-line no-use-before-define
import React from 'react'
import {
  Collapse,
  ListItem
} from '@material-ui/core'
import { ExpandLess, ExpandMore } from '@material-ui/icons'
import { useTheme } from '@material-ui/core/styles'

import { StyledListItemIcon, StyledText } from './MenuStyles'

type MenuItemProps = {
    open: boolean;
    title: string;
    onChange: React.MouseEventHandler<HTMLDivElement>;
    children: React.ReactNode;
    icon: React.ReactNode;
}

function MenuItem ({ open, onChange, title, children, icon }: MenuItemProps) {
  const theme = useTheme()
  return (
      <>
        <ListItem button onClick={onChange}>
          <StyledListItemIcon open={open} theme={theme} className=''>
            {open ? <ExpandLess /> : <ExpandMore />}
            {icon}
          </StyledListItemIcon>
          <StyledText primary={title} open={open} />
        </ListItem>
        <Collapse in={open} timeout="auto" unmountOnExit>
            {children}
        </Collapse>
      </>
  )
}

export default MenuItem
