// eslint-disable-next-line no-use-before-define
import React from 'react'
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Badge
} from '@material-ui/core'
import { ListItemIconProps } from '@material-ui/core/ListItemIcon'
import { ListItemTextProps } from '@material-ui/core/ListItemText'
import { styled, Theme } from '@material-ui/core/styles'
import { NavLink } from 'react-router-dom'
import { Omit } from '@material-ui/types'

interface StyledListItemIconProps { open:boolean; theme: Theme;className:string;}
interface StyledTextProps { open:boolean }

export const Wrapper = styled(List)(({ theme }) => ({
  backgroundColor: 'transparent',
  color: '#3edbf0'
}))

export const SubMenuWrapper = styled(List)(({ theme }) => ({
  paddingLeft: theme.spacing(7),
  paddingTop: theme.spacing(1),
  color: 'inherit',

  [theme.breakpoints.up('sm')]: {
    paddingLeft: theme.spacing(3)
  }
}))

export const StyledListItemIcon = styled(
  ({ open, theme, className, ...other }: StyledListItemIconProps & Omit<ListItemIconProps, keyof StyledListItemIconProps>) => (
    <ListItemIcon className={className} {...other} />
  )
)(({
  color: (props: StyledListItemIconProps) => props.open ? '#fff' : 'inherit',

  '&.sub-item': {
    minWidth: 'fit-content',
    marginRight: (props: StyledListItemIconProps) => props.theme.spacing(1.5),
    marginLeft: (props: StyledListItemIconProps) => props.theme.spacing(0.8)
  }
}))

export const StyledText = styled(
  ({ open, ...other }: StyledTextProps & Omit<ListItemTextProps, keyof StyledTextProps>) => (
    <ListItemText {...other} />
  )
)(({
  color: (props:StyledTextProps) => props.open ? '#fff' : ''
}))

export const StyledRouteLink = styled(NavLink)({
  textDecoration: 'none',
  color: 'inherit',

  '&.active-item': {
    color: '#fb9300',

    '& .MuiListItemIcon-root': {
      color: '#fb9300'
    }
  }
})

export const StyledSubItemContainer = styled(ListItem)({
  paddingLeft: '3px'
})

export const StyledBadge = styled(Badge)(({ theme }) => ({
  backgroundColor: '#fb9300',
  color: '#fff',
  borderRadius: '50%',

  '& .MuiBadge-badge': {
    position: 'relative',
    padding: '12px 6px'
  },

  '& .MuiBadge-anchorOriginTopRightRectangle': {
    transform: 'none'
  }
}))
