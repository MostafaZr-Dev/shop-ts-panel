// eslint-disable-next-line no-use-before-define
import React from 'react'
import { Grid, Typography } from '@material-ui/core'
import { useTheme } from '@material-ui/core/styles'
import useMediaQuery from '@material-ui/core/useMediaQuery'

import { Wrapper, Header } from './SidebarStyles'
import Menu from './components/menu'
import SidebarDrawer from './components/darwer'

interface SidebarProps {
  open: boolean;
  closeDrawer: () => void;
}

function Sidebar ({ open, closeDrawer }: SidebarProps) {
  const theme = useTheme()
  const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'))

  return (
    <>
      {isSmallDevice && (
        <SidebarDrawer open={open} onClose={closeDrawer}>
          <Wrapper>
            <Header>
              <Typography>پنل مدیریت فروشگاه</Typography>
            </Header>
            <Menu />
          </Wrapper>
        </SidebarDrawer>
      )}
      {!isSmallDevice && (
        <Grid item xs={8} sm={8} md={3} lg={2}>
          <Wrapper>
            <Header>
              <Typography>پنل مدیریت فروشگاه</Typography>
            </Header>

            <Menu />
          </Wrapper>
        </Grid>
      )}
    </>
  )
}

export default Sidebar
