import { AppBar, Grid, Box, Avatar } from '@material-ui/core'
import { styled } from '@material-ui/core/styles'

export const Header = styled(AppBar)({
  backgroundColor: '#fff',
  color: '#333'
})

export const ContentWrapper = styled(Grid)(({ theme }) => ({
  padding: theme.spacing(3)
}))

export const RightHeader = styled(Box)(({ theme }) => ({
  display: 'flex',
  flex: 1
}))

export const UserAvatar = styled(Avatar)(({ theme }) => ({
  width: '30px',
  height: '30px'
}))
