// eslint-disable-next-line no-use-before-define
import React from 'react'
import { Grid, IconButton, Toolbar, Box, Badge } from '@material-ui/core'
import { Menu, Notifications } from '@material-ui/icons'
import { useTheme } from '@material-ui/core/styles'
import useMediaQuery from '@material-ui/core/useMediaQuery'

import { Header, ContentWrapper, RightHeader, UserAvatar } from './ContentStyles'

type ContentProps = {
    children: React.ReactNode;
    toggleDrawer: React.MouseEventHandler<HTMLButtonElement>;
}

function Content ({ children, toggleDrawer }: ContentProps) {
  const theme = useTheme()
  const isSmallDevice = useMediaQuery(theme.breakpoints.down('sm'))

  return (
    <Grid item xs={12} sm={12} md={9} lg={10}>
      <Grid container direction="column">
        <Grid item xs={12} md={12}>
          <Header position="static">
            <Toolbar>
              <RightHeader>
                {isSmallDevice && (
                  <IconButton
                    edge="start"
                    color="inherit"
                    aria-label="menu"
                    onClick={toggleDrawer}
                  >
                    <Menu />
                  </IconButton>
                )}
              </RightHeader>
              <Box>
                <IconButton>
                  <Badge
                    badgeContent={4}
                    anchorOrigin={{
                      vertical: 'top',
                      horizontal: 'left'
                    }}
                    color="error"
                    >
                    <Notifications />
                  </Badge>
                </IconButton>
                <IconButton>
                  <UserAvatar>

                  </UserAvatar>
                </IconButton>
              </Box>
            </Toolbar>
          </Header>
        </Grid>
        <ContentWrapper item xs={12} md={12}>
          {children}
        </ContentWrapper>
      </Grid>
    </Grid>
  )
}

export default Content
