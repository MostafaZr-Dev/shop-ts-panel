import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios'

class HttpService {
  private axios: AxiosInstance;
  private baseUrl: string;

  constructor () {
    this.baseUrl = 'http://localhost:5000'

    this.axios = axios.create({
      baseURL: this.baseUrl
    })
  }

  public get<T, R = AxiosResponse<T>> (endpoint: string, configs?: AxiosRequestConfig): Promise<R> {
    return this.axios.get(endpoint, configs)
  }

  public post<T, B, R = AxiosResponse<T>> (endpoint: string, data?: B, configs?: AxiosRequestConfig): Promise<R> {
    return this.axios.post(endpoint, data, configs)
  }

  public patch<T, B, R = AxiosResponse<T>> (endpoint: string, data?: B, configs?: AxiosRequestConfig): Promise<R> {
    return this.axios.put(endpoint, data, configs)
  }

  public delete (endpoint: string, configs: object) {
    return this.axios.delete(endpoint, configs)
  }
}

export default new HttpService()
