
export default class CurrencyService {
  public static formatIRR (value: number): string {
    return new Intl.NumberFormat('fa-IR', { style: 'currency', currency: 'IRR' }).format(value)
  }
}
