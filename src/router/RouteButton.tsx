// eslint-disable-next-line no-use-before-define
import React from 'react'
import { Box, Button } from '@material-ui/core'
import { styled } from '@material-ui/core/styles'
import { Link } from 'react-router-dom'

export const ButtonContainer = styled(Box)(({ theme }) => ({
  display: 'flex'
}))

export const StyledButton = styled(Button)(({ theme }) => ({
  backgroundColor: '#fb3900',
  color: '#fff',

  '&:hover': {
    backgroundColor: 'rgba(251,57,0,0.6)'
  }
}))

type RouteButtonProps = {
    to: string;
    title: string;
}

function RouteButton ({ to, title }:RouteButtonProps) {
  return (
        <ButtonContainer>
            <Link to={to} style={{ textDecoration: 'none' }}>
                <StyledButton variant='contained'>{title}</StyledButton>
            </Link>
        </ButtonContainer>
  )
}

export default RouteButton
