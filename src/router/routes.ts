import Panel from '../pages/panel'
import Products from '../components/panel/products'
import CreateProduct from '../components/panel/createProduct'
import CreateCategory from '../components/panel/createCategory'
import Categories from '../components/panel/categories'
import ProductsOffer from '../components/panel/productsOffer'
import Orders from '../components/panel/orders'
import OrdersDetails from '../components/panel/orderDetails'
import Payments from '../components/panel/payments'
import Coupons from '../components/panel/coupons'
import CreateCoupon from '../components/panel/createCoupon'
import Settings from '../components/panel/settings'
import CreateSettings from '../components/panel/createSettings'

interface RouteItem {

  path: string;
  component: any;
  exact: boolean;
  routes: RouteItem[];

}

const routes: RouteItem[] = [
  {
    path: '/dashboard',
    component: Panel,
    exact: false,
    routes: [
      {
        path: '/products/create',
        component: CreateProduct,
        exact: true,
        routes: []
      },
      {
        path: '/products/offers',
        component: ProductsOffer,
        exact: true,
        routes: []
      },
      {
        path: '/products',
        component: Products,
        exact: true,
        routes: []
      },
      {
        path: '/categories/create',
        component: CreateCategory,
        exact: true,
        routes: []
      },
      {
        path: '/categories',
        component: Categories,
        exact: true,
        routes: []
      },
      {
        path: '/orders/:orderID',
        component: OrdersDetails,
        exact: true,
        routes: []
      },
      {
        path: '/orders',
        component: Orders,
        exact: true,
        routes: []
      },
      {
        path: '/financial/coupons/create',
        component: CreateCoupon,
        exact: true,
        routes: []
      },
      {
        path: '/financial/coupons',
        component: Coupons,
        exact: true,
        routes: []
      },
      {
        path: '/financial/payments/:paymentID',
        component: OrdersDetails,
        exact: true,
        routes: []
      },
      {
        path: '/financial/payments',
        component: Payments,
        exact: true,
        routes: []
      },
      {
        path: '/settings/create',
        component: CreateSettings,
        exact: true,
        routes: []
      },
      {
        path: '/settings',
        component: Settings,
        exact: true,
        routes: []
      }

    ]
  }
]

export default routes
