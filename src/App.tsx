// eslint-disable-next-line no-use-before-define
import React from 'react'
import { BrowserRouter as Router } from 'react-router-dom'

import { AppRoutes } from './router'
import RTL from './theme/rtl'

function App () {
  return (
    <Router>
      <RTL>
        <AppRoutes />
      </RTL>
    </Router>
  )
}

export default App
