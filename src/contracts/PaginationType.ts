
export default interface PaginationType {

    page: number;
    perPage: number;
    totalPages: number;
    totalItems: number;

}
