import AttributeItemType from './AttributeItemType'

export default interface ProductAttributes {

    title: string;
    attributes: AttributeItemType[];

}
