export interface VariationItemType {
    title: string;
    value: string;
}

export interface VariationType {

    hash: string;
    title: string;
    name: string;
    type: string;
    items: VariationItemType[];

}
