export default interface CategoryItemType {
    id: string;
    title: string;
    slug: string;

}
