import CouponStatus from './CouponStatus'

export default interface CouponType {
    id: string;
    code: string;
    percent: number;
    limit: number;
    used: number;
    expiresAt: string;
    constraints: object;
    status: CouponStatus;

}
