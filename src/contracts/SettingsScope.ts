/* eslint-disable no-unused-vars */

enum SettingsScope {
    PUBLIC,
    PRIVATE
}

export default SettingsScope
