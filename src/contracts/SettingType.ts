import SettingsScope from './SettingsScope'

export default interface SettingType {

    id: string;
    title: string;
    key: string;
    value: string;
    version: string;
    scope: SettingsScope;

}
