import OrderStatus from 'components/panel/orders/OrderStatus'
import ProductType from './ProductType'

type IUser = {
    firstName: string;
    lastName: string;
    email: string;
}

export interface OrderLineType {
    product: ProductType;
    price: number;
    quantity: number;
    discountedPrice: number;
}

export default interface OrderType {
    id: string;
    user: IUser;
    totalPrice: number;
    finalPrice: number;
    orderLines: OrderLineType[];
    status: OrderStatus;
    createdAt: string;
    updatedAt: string;
}
