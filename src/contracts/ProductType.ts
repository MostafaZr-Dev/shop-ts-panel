export default interface ProductType {
    id: string;
    title: string;
    price: number;
    discountedPrice: number;
    stock: number;
    thumbnail?: string;
    gallery?: string[];
    category: string;
    attributes: object[];
    variations: object[];
    priceVariations: object[];
    createdAt: string;
    updatedAt: string;
    status: number;

}
