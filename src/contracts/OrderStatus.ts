/* eslint-disable no-unused-vars */

enum OrderStatus {
    INIT, // ثبت شده
    PAID, // پرداخت شده
    CONFIRMED, // تایید شده
    INVENTORY, // در حال پردازش (انبار)
    READY, // آماده ارسال
    SENT, // ارسال شده
    DELIVERED, // تحویل داده شده
    CANCELED, // لغو شده
    REFUNDED // مرجوع شده
}

export default OrderStatus
