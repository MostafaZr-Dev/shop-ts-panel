import PaymentStatus from './PaymentStatus'

type UserType = {
    firstName: string;
    lastName: string;
    email: string;
}

type OrderType = {
    id: string;
}

export default interface PaymentType {
    id: string;
    user: UserType;
    order: OrderType;
    amount: number;
    method: string;
    reserve: string;
    reference: string;
    createdAt: string;
    updatedAt: string;
    status: PaymentStatus

}
