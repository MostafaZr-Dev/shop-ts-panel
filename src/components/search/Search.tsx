// eslint-disable-next-line no-use-before-define
import React, { useCallback } from 'react'
import { TextField } from '@material-ui/core'
import { debounce } from 'lodash'

import { SearchContainer } from './SearchStyles'

type SearchProps = {
    label: string;
    defaultValue: string;
    onChange?: (keyword: string) => void;
}

function Search ({ label, defaultValue, onChange }: SearchProps) {
  const performSearch = useCallback(debounce(
    (keyword:string) => {
      if (onChange) {
        onChange(keyword)
      }
    }
    , 1000), [])

  const handleChange = useCallback(
    (e:React.ChangeEvent<HTMLInputElement>) => {
      performSearch(e.target.value)
    },
    []
  )

  return (
        <SearchContainer>
          <TextField
            id='app_search_section'
            label={label}
            defaultValue={defaultValue}
            variant='outlined'
            onChange={handleChange}
            fullWidth
        />
        </SearchContainer>
  )
}

export default Search
