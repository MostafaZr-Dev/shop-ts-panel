import { Box } from '@material-ui/core'
import { styled } from '@material-ui/core/styles'

export const SearchContainer = styled(Box)(({ theme }) => ({
  display: 'flex',
  margin: theme.spacing(2, 'auto')
}))
