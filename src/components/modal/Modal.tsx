// eslint-disable-next-line no-use-before-define
import React from 'react'
import { Dialog, DialogTitle, DialogContent, DialogActions } from '@material-ui/core'

type ModalProps = {
    open: boolean;
    title: string;
    children: React.ReactNode;
    actions?: React.ReactNode;
    onClose?: (e:React.MouseEvent) => void;
}

function Modal ({ open, title, children, actions, onClose }:ModalProps) {
  return (
        <Dialog open={open} onClose={onClose} maxWidth="md" fullWidth>
            <DialogTitle>{title}</DialogTitle>
            <DialogContent>
                {children}
            </DialogContent>
            <DialogActions>
                {actions}
            </DialogActions>
        </Dialog>
  )
}

export default Modal
