import * as Yup from 'yup'

const settingValidationSchema = Yup.object({
  title: Yup.string().required('فیلد الزامی است!'),
  key: Yup.string().required('فیلد الزامی است!'),
  value: Yup.string().required('فیلد الزامی است!'),
  version: Yup.string().required('فیلد الزامی است!')
})

export default settingValidationSchema
