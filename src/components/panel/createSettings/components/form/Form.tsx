// eslint-disable-next-line no-use-before-define
import React, { useMemo } from 'react'
import {
  Grid,
  TextField,
  Button,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio
} from '@material-ui/core'
import { Save } from '@material-ui/icons'
import { Formik, FormikHelpers } from 'formik'

import SettingsScope from 'contracts/SettingsScope'
import SettingFormValues from '../../SettingFormValues'
import settingValidationSchema from './FormSchema'

type FormProps = {
    onSubmit: (values:SettingFormValues, formikHelpers:FormikHelpers<SettingFormValues>) => void | Promise<any>;
}

function Form ({ onSubmit } : FormProps) {
  const initValues : SettingFormValues = useMemo(() => ({
    title: '',
    key: '',
    value: '',
    scope: SettingsScope.PUBLIC,
    version: ''
  }), [])

  return (
    <Formik
    initialValues={initValues}
    onSubmit={onSubmit}
    validationSchema={settingValidationSchema}
  >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        submitForm,
        setFieldValue
      }) => (
          <Grid container spacing={2}>
              <Grid item xs={12} md={6}>
                  <TextField
                      id="coupon-title"
                      label="عنوان"
                      variant="outlined"
                      name="title"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.title}
                      error={!!errors.title}
                      helperText={errors.title && touched.title ? errors.title : null}
                      fullWidth
                  />
              </Grid>
              <Grid item xs={12} md={6}>
                  <TextField
                      id="coupon-key"
                      label="کلید"
                      variant="outlined"
                      name="key"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.key}
                      error={!!errors.key}
                      helperText={errors.key && touched.key ? errors.key : null}
                      fullWidth
                  />
              </Grid>
              <Grid item xs={12} md={6}>
                  <TextField
                      id="coupon-value"
                      label="مقدار"
                      variant="outlined"
                      name="value"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.value}
                      error={!!errors.value}
                      helperText={errors.value && touched.value ? errors.value : null}
                      fullWidth
                  />
              </Grid>
              <Grid item xs={12} md={6}>
                  <TextField
                      id="coupon-version"
                      label="نسخه"
                      variant="outlined"
                      name="version"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.version}
                      error={!!errors.version}
                      helperText={errors.version && touched.version ? errors.version : null}
                      fullWidth
                  />
              </Grid>
              <Grid item xs={12} md={6}>
                <FormControl component="fieldset" fullWidth>
                    <FormLabel component="legend">نوع</FormLabel>
                    <RadioGroup row aria-label="setting-scope" name="scope" value={values.scope.toString()} onChange={(e:React.ChangeEvent<HTMLInputElement>) => {
                      setFieldValue('scope', e.currentTarget.value)
                    }}>
                        <FormControlLabel value={SettingsScope.PUBLIC.toString()} control={<Radio />} label="عمومی" />
                        <FormControlLabel value={SettingsScope.PRIVATE.toString()} control={<Radio />} label="خصوصی" />
                    </RadioGroup>
                </FormControl>
              </Grid>
              <Grid item xs={12} md={12}>
                <Button variant="contained" color="default" startIcon={<Save />} onClick={submitForm}>ذخیره سازی</Button>
            </Grid>
          </Grid>
      )}
    </Formik>
  )
}

export default Form
