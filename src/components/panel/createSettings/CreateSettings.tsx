// eslint-disable-next-line no-use-before-define
import React, { useCallback, useEffect } from 'react'
import { useHistory } from 'react-router-dom'

import Container from '../container'
import Form from './components/form'
import SettingFormValue from './SettingFormValues'
import usePostAPI from 'hooks/usePostAPI'
import { LinearProgress } from 'components/progress'
import Toast from 'components/toast'

function CreateSettings () {
  const [createSettingResponse, createSettingAPI] = usePostAPI({
    url: '/api/v1/settings'
  })

  const history = useHistory()

  const { isLoading, success } = createSettingResponse

  useEffect(() => {
    let timeout: ReturnType<typeof setTimeout>

    if (success) {
      timeout = setTimeout(() => {
        history.push('/dashboard/settings')
      }, 2000)
    }

    return () => {
      clearTimeout(timeout)
    }
  }, [success])

  const saveSetting = useCallback((values: SettingFormValue) => {
    console.log({ values })

    createSettingAPI({
      title: values.title,
      key: values.key,
      value: values.value,
      version: values.version,
      scope: values.scope
    })
  }, [])

  return (
        <Container title='ایجاد تنظیمات'>
          {isLoading && <LinearProgress />}
          <Toast open={success} type='success' message='تنظیمات با موفقیت ایجاد شد' />
          <Form onSubmit={saveSetting}/>
        </Container>
  )
}

export default CreateSettings
