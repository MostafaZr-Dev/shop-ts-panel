
import SettingsScope from 'contracts/SettingsScope'

export default interface SettingFormValue {

    title: string;
    key: string;
    value: string;
    scope: SettingsScope;
    version: string;

}
