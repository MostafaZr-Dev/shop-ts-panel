import ProductAttributesType from 'contracts/ProductAttributesType'
import { VariationType } from 'contracts/VariationsType'
import { PriceVariationItem } from './components/form/PriceVariations/PriceVariations'

export default interface ProductFormValues {
    title: string;
    slug: string;
    price: number | undefined;
    discountedPrice: number;
    stock: number;
    category: string | undefined;
    attributes: ProductAttributesType[];
    variations: VariationType[];
    priceVariations: PriceVariationItem[];
    thumbnail: File | undefined;
    gallery: FileList | undefined;
}
