// eslint-disable-next-line no-use-before-define
import React, { useRef, useCallback } from 'react'
import { FormControl, InputLabel, Box, Button, FormHelperText } from '@material-ui/core'
import { styled } from '@material-ui/core/styles'

const StyledFormControl = styled(FormControl)(({ theme }) => ({
  '& .label': {
    position: 'relative',
    transform: 'none',
    marginBottom: theme.spacing(2)
  },

  '& .file-btn': {
    backgroundColor: '#005a8d',
    color: '#fff',
    marginBottom: theme.spacing(1),

    '&:hover': {
      backgroundColor: '#fb9300'
    }
  },

  '& .create-btn': {
    backgroundColor: '#fb9300',

    '&:hover': {
      backgroundColor: 'rgb(251 147 0 / 69%)'
    }
  }
}))

type ImageUploadProps = {
    title: string;
    btnTitle: string;
    id: string;
    name: string;
    helperText?: string | undefined;
    multiple?: boolean | undefined;
    onChange: (files: FileList | null) => void
}

function ImageUpload ({ title, id, name, btnTitle, helperText, multiple, onChange }: ImageUploadProps) {
  const imageRef = useRef<HTMLInputElement>(null)

  const handleChangeImage = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      const files = e.currentTarget.files

      onChange(files)
    },
    []
  )

  const handleFileBtnClick = useCallback(
    () => {
      if (imageRef.current !== null) {
        imageRef.current.click()
      }
    },
    []
  )

  return (
        <StyledFormControl fullWidth>
                            <InputLabel className="label">{title}</InputLabel>
                            <input
                                type="file"
                                name={name}
                                id={id}
                                aria-describedby="my-helper-text"
                                hidden
                                ref={imageRef}
                                onChange={handleChangeImage}
                                multiple={multiple}
                            />
                            <Box>
                                <Button
                                className="file-btn"
                                onClick={handleFileBtnClick}
                                >
                                {btnTitle}
                                </Button>
                            </Box>
                            {helperText && <FormHelperText id="my-helper-text">
                                {helperText}
                            </FormHelperText>}
                        </StyledFormControl>
  )
}

export default ImageUpload
