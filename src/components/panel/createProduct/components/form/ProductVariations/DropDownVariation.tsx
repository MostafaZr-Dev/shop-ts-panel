// eslint-disable-next-line no-use-before-define
import React, { useState, useCallback } from 'react'
import { Box, Grid, Typography, Divider, Button, List, TextField, FormControl } from '@material-ui/core'

import { VariationItemType } from 'contracts/VariationsType'
import Modal from 'components/modal'
import DropDownVariationsItem from './DropDownVariationItem'
import { DropDownVariationContainer } from './ProductVariationStyles'

type DropDownVariationProps = {
  title: string;
  hash: string;
  items: VariationItemType[],
  onItemAdded: (hash:string, title:string, value: string) => void
}

function DropDownVariation ({ title, hash, items, onItemAdded } : DropDownVariationProps) {
  const [isDropDownDialogOpen, setIsDropDownDialogOpen] = useState<boolean>(false)
  const [newDropDown, setNewDropDown] = useState<{title:string;value:string;}>({
    title: '',
    value: ''
  })

  const openAddDropDownDialog = useCallback(
    () => {
      setIsDropDownDialogOpen(true)
    },
    []
  )

  const closeDropDownDialog = useCallback(
    () => {
      setIsDropDownDialogOpen(false)
    },
    []
  )

  const handleChangeDropDown = useCallback(
    (e:React.ChangeEvent<HTMLInputElement>) => {
      setNewDropDown((prev) => ({
        ...prev,
        [e.target.name]: e.target.value
      }))
    },
    []
  )

  const addDropDownItem = () => {
    onItemAdded(hash, newDropDown.title, newDropDown.value)
    closeDropDownDialog()
    setNewDropDown({ title: '', value: '' })
  }

  return (
        <DropDownVariationContainer>
            <Typography>{title}</Typography>
            <Divider />
            <Box>
                <List>
                    {items.map((item: VariationItemType, i) => (<DropDownVariationsItem key={i} title={item.title} value={item.value} />))}
                </List>
                <Button variant="contained" onClick={openAddDropDownDialog}>اضافه کردن مقدار جدید</Button>
            </Box>
            <Modal
            open={isDropDownDialogOpen}
            title="اضافه کردن متغیر محصول"
            onClose={closeDropDownDialog}
            actions={<>
                <Button variant="contained" color="secondary" onClick={closeDropDownDialog}>لغو</Button>
                <Button variant="contained" color="primary" onClick={addDropDownItem}>ایجاد</Button>
            </>}
            >
                <Grid container direction="column" spacing={2}>
                    <Grid item>
                        <FormControl fullWidth>
                            <TextField
                                id="color_title"
                                name="title"
                                variant="outlined"
                                value={newDropDown.title}
                                label="عنوان آیتم"
                                onChange={handleChangeDropDown}
                                fullWidth
                            />
                        </FormControl>
                    </Grid>
                    <Grid item>
                        <FormControl fullWidth>
                            <TextField
                                id="color_value"
                                name="value"
                                variant="outlined"
                                value={newDropDown.value}
                                label="مقدار آیتم"
                                onChange={handleChangeDropDown}
                                fullWidth
                            />
                        </FormControl>
                    </Grid>
                </Grid>
            </Modal>
    </DropDownVariationContainer>
  )
}

export default DropDownVariation
