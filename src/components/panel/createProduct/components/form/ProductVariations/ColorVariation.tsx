// eslint-disable-next-line no-use-before-define
import React, { useState, useCallback } from 'react'
import { Box, Grid, Typography, Divider, Button, List, TextField, FormControl } from '@material-ui/core'

import { VariationItemType } from 'contracts/VariationsType'
import ColorVariationsItem from './ColorVariationsItem'
import Modal from 'components/modal'
import { ColorVariationContainer } from './ProductVariationStyles'

type ColorVariationProps = {

    title: string;
    hash: string;
    items: VariationItemType[],
    onAddColor: (hash:string, colorTitle:string, colorValue: string) => void

}

function ColorVariation ({ title, hash, items, onAddColor }: ColorVariationProps) {
  const [isColorDialogOpen, setIsColorDialogOpen] = useState<boolean>(false)
  const [newColor, setNewColor] = useState<{title:string;value:string;}>({
    title: '',
    value: '#000'
  })

  const openAddColorDialog = useCallback(
    () => {
      setIsColorDialogOpen(true)
    },
    []
  )

  const closeColorDialog = useCallback(
    () => {
      setIsColorDialogOpen(false)
    },
    []
  )

  const handleChangeColor = useCallback(
    (e:React.ChangeEvent<HTMLInputElement>) => {
      setNewColor((prev) => ({
        ...prev,
        [e.target.name]: e.target.value
      }))
    },
    []
  )

  const addColorItem = () => {
    onAddColor(hash, newColor.title, newColor.value)
    closeColorDialog()
    setNewColor({ title: '', value: '' })
  }

  return (
        <ColorVariationContainer>
            <Typography>{title}</Typography>
            <Divider />
            <Box>
                <List>
                    {items.map((item: VariationItemType, i) => (<ColorVariationsItem key={i} title={item.title} value={item.value} />))}
                </List>
                <Button variant="contained" onClick={openAddColorDialog}>اضافه کردن رنگ جدید</Button>
            </Box>
            <Modal
            open={isColorDialogOpen}
            title="اضافه کردن متغیر محصول"
            onClose={closeColorDialog}
            actions={<>
                <Button variant="contained" color="secondary" onClick={closeColorDialog}>لغو</Button>
                <Button variant="contained" color="primary" onClick={addColorItem}>ایجاد</Button>
            </>}
            >
                <Grid container direction="column" spacing={2}>
                    <Grid item>
                        <FormControl fullWidth>
                            <TextField
                                id="color_title"
                                name="title"
                                variant="outlined"
                                value={newColor.title}
                                label="عنوان رنگ"
                                onChange={handleChangeColor}
                                fullWidth
                            />
                        </FormControl>
                    </Grid>
                    <Grid item>
                        <FormControl fullWidth>
                            <TextField
                                id="color_value"
                                name="value"
                                type="color"
                                variant="outlined"
                                value={newColor.value}
                                label="کد رنگ"
                                onChange={handleChangeColor}
                                fullWidth
                            />
                        </FormControl>
                    </Grid>
                </Grid>
            </Modal>
    </ColorVariationContainer>
  )
}

export default ColorVariation
