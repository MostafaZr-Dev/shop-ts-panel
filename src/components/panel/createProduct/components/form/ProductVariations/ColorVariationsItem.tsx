// eslint-disable-next-line no-use-before-define
import React from 'react'
import { ListItem, ListItemText, IconButton, ListItemIcon, ListItemSecondaryAction } from '@material-ui/core'
import { Palette, Delete } from '@material-ui/icons'
import { styled, Theme } from '@material-ui/core/styles'

import { VariationItemType } from 'contracts/VariationsType'

type ColorIconProps = {
  theme: Theme;
  selectedColor: string;
}

const ColorIcon = styled(Palette)(({ theme, selectedColor }:ColorIconProps) => ({
  color: selectedColor
}))

function ColorVariationsItem ({ title, value }:VariationItemType) {
  return (
        <ListItem>
          <ListItemIcon>
              <IconButton>
                  <Delete color="secondary" />
              </IconButton>
              <IconButton>
                  <ColorIcon selectedColor={value} />
              </IconButton>
            </ListItemIcon>
            <ListItemText primary={title} />
            <ListItemSecondaryAction>

            </ListItemSecondaryAction>
        </ListItem>
  )
}

export default ColorVariationsItem
