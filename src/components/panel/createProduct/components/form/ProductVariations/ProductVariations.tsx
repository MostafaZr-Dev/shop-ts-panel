// eslint-disable-next-line no-use-before-define
import React, { useEffect, useState, useCallback } from 'react'
import {
  Box,
  Button,
  TextField,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
  Grid
} from '@material-ui/core'
import { styled } from '@material-ui/core/styles'

import Modal from 'components/modal'
import { VariationType } from 'contracts/VariationsType'
import { generateID } from 'services/hashService'
import ColorVariation from './ColorVariation'
import DropDownVariation from './DropDownVariation'

const AddVariationButton = styled(Button)(({ theme }) => ({

  backgroundColor: '#005a8d',
  color: '#fff',

  '&:hover': {
    backgroundColor: '#fb9300'
  }

}))

const StyledVariationTypeContainer = styled((props) => <FormControl {...props}/>)(({ theme }) => ({
  border: '2px solid #ddd',
  marginTop: theme.spacing(2)
}))

type ProductVariantsProps = {
  onChangeVariations: (variations: VariationType[]) => void
}

function ProductVariants ({ onChangeVariations }:ProductVariantsProps) {
  const [isAddDialogOpen, setIsAddDialogOpen] = useState<boolean>(false)
  const [variations, setVariations] = useState<VariationType[]>([])
  const [newVariation, setNewVariation] = useState<{title:string;type:string;name: string;}>({
    title: '',
    type: 'color',
    name: ''
  })

  useEffect(() => {
    onChangeVariations(variations)
  }, [variations])

  const openAddDialog = useCallback(
    () => {
      setIsAddDialogOpen(true)
    },
    []
  )

  const closeAddDialog = useCallback(
    () => {
      setIsAddDialogOpen(false)
    },
    []
  )

  const addVariation = () => {
    setVariations(prev => ([
      ...prev,
      {
        hash: generateID(),
        title: newVariation.title,
        type: newVariation.type,
        name: newVariation.name,
        items: []
      }
    ]))

    closeAddDialog()
    setNewVariation({ title: '', type: 'color', name: '' })
  }

  const handleChangeVariation = useCallback(
    (e:React.ChangeEvent<HTMLInputElement>) => {
      console.log(e.currentTarget.name)
      setNewVariation((prev) => ({
        ...prev,
        [e.target.name]: e.target.value
      }))
    },
    []
  )

  const addVariantItem = (hash: string, title:string, value: string) => {
    setVariations(variations.map((variation) => {
      if (variation.hash === hash) {
        return {
          ...variation,
          items: [
            ...variation.items,
            {
              title: title,
              value: value
            }
          ]
        }
      }

      return variation
    }))
  }

  return (
    <Box>
        <Box>
            {variations.map((variation) => {
              if (variation.type === 'color') {
                return <ColorVariation
                            key={variation.hash}
                            hash={variation.hash}
                            title={variation.title}
                            items={variation.items}
                            onAddColor={addVariantItem}
                        />
              }
              return <DropDownVariation
                          key={variation.hash}
                          hash={variation.hash}
                          title={variation.title}
                          items={variation.items}
                          onItemAdded={addVariantItem}/>
            })}
        </Box>
        <AddVariationButton variant="contained" color="primary" onClick={openAddDialog}>اضافه کردن متغیرهای محصول</AddVariationButton>
        <Modal
            open={isAddDialogOpen}
            title="اضافه کردن متغیر محصول"
            onClose={closeAddDialog}
            actions={<>
                <Button variant="contained" color="secondary" onClick={closeAddDialog}>لغو</Button>
                <Button variant="contained" color="primary" onClick={addVariation}>ایجاد</Button>
            </>}
        >
          <Grid container spacing={2}>
              <Grid item xs={12} md={12}>
                <TextField
                  id="variation_title"
                  name="title"
                  variant="outlined"
                  value={newVariation.title}
                  label="عنوان متغیر محصول"
                  onChange={handleChangeVariation}
                  fullWidth
                />
              </Grid>
              <Grid item xs={12} md={12}>
                <TextField
                  id="variation_name"
                  name="name"
                  variant="outlined"
                  value={newVariation.name}
                  label="نام متغیر محصول"
                  placeholder="مثلا color-size-material"
                  onChange={handleChangeVariation}
                  fullWidth
                />
              </Grid>
          </Grid>
          <StyledVariationTypeContainer component="fieldset">
                <FormLabel component="legend">نوع متغیر محصول</FormLabel>
                <RadioGroup aria-label="variation-type" name="type" value={newVariation.type} onChange={handleChangeVariation} >
                    <FormControlLabel value="color" control={<Radio />} label="رنگ" />
                    <FormControlLabel value="dropdown" control={<Radio />} label="لیست کشویی" />
                </RadioGroup>
          </StyledVariationTypeContainer>
        </Modal>
    </Box>
  )
}

export default ProductVariants
