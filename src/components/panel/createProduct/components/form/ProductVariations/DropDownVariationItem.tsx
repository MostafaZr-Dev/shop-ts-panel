// eslint-disable-next-line no-use-before-define
import React from 'react'
import { ListItem, ListItemText } from '@material-ui/core'

import { VariationItemType } from 'contracts/VariationsType'

function DropDownVariationItem ({ title, value } :VariationItemType) {
  return (
        <ListItem>
            <ListItemText primary={title} secondary={value} />
        </ListItem>
  )
}

export default DropDownVariationItem
