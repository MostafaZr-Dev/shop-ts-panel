import { Box } from '@material-ui/core'
import { styled } from '@material-ui/core/styles'

export const ColorVariationContainer = styled(Box)(({ theme }) => ({
  paddingBottom: theme.spacing(2)
}))

export const DropDownVariationContainer = styled(Box)(({ theme }) => ({
  paddingBottom: theme.spacing(2)
}))
