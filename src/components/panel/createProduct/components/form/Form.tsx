// eslint-disable-next-line no-use-before-define
import React, { useRef, useState, useCallback } from 'react'
import { Grid, TextField, Select, FormControl, InputLabel, Button, FormHelperText } from '@material-ui/core'
import { Formik, FormikHelpers, FormikProps } from 'formik'
import { Save } from '@material-ui/icons'
import { debounce } from 'lodash'

import productValidationSchema from './validationSchema'
import CategoryItemType from 'contracts/CategoryItemType'
import ProductAttributesType from 'contracts/ProductAttributesType'
import ImageUpload from './ImageUpload'
import PreviewImage from './PreviewImage'
import ProductAttribute from './ProductAttribute'
import Section from './Section'
import ProductVariations from './ProductVariations'
import { generateID } from 'services/hashService'
import currencyService from 'services/currencyService'
import ProductFormValues from '../../ProductFormValues'
import PriceVariations from './PriceVariations'

type FormProps = {
    categories: CategoryItemType[];
    attributes: ProductAttributesType[];
    onSubmit: (values:ProductFormValues, formikHelpers:FormikHelpers<ProductFormValues>) => void | Promise<any>;
    onUpdateCategory: (catID: string) => void;
}

type ThumbnailValue = {
    id:string;
    src: string;
    name: string;
}

function Form ({ categories, attributes, onSubmit, onUpdateCategory }:FormProps) {
  const [thumbnail, setThumbnail] = useState<ThumbnailValue | null>(null)
  const [galleryImages, setGalleryImages] = useState<ThumbnailValue[] | null>(null)

  const initValues : ProductFormValues = {
    title: '',
    slug: '',
    price: undefined,
    discountedPrice: 0,
    stock: 0,
    category: '',
    attributes: attributes,
    variations: [],
    priceVariations: [],
    thumbnail: undefined,
    gallery: undefined
  }

  const formikRef = useRef<FormikProps<ProductFormValues>>()

  const handleChangeThumbnail = (files: FileList | null) => {
    let file : File | null = null

    if (files !== null) {
      file = files[0]
    }

    if (!file) {
      return
    }

    setThumbnail({
      id: generateID(),
      src: URL.createObjectURL(file),
      name: file.name
    })

    formikRef.current?.setFieldValue('thumbnail', file)
  }

  const handleChangeGallery = (files: FileList | null) => {
    if (!files) {
      return
    }

    const images :ThumbnailValue[] = Array.from(files).map(file => ({
      id: generateID(),
      name: file.name,
      src: URL.createObjectURL(file)
    }))

    setGalleryImages(images)
    formikRef.current?.setFieldValue('gallery', files)
  }

  const onChangeCategory = (categoryID:any) => {
    onUpdateCategory(categoryID)
  }

  const handleAttributeChange = (e:React.ChangeEvent<HTMLInputElement>, attrID: string) => {
    updateAttributeByHash(attrID, e.currentTarget.value)
  }

  const updateAttributeByHash = debounce((hash: string, value: string) => {
    const newProductAttributes = attributes.map(group => {
      const newAttributes = group.attributes.map(attr => {
        if (attr.hash === hash) {
          return {
            ...attr,
            value
          }
        }

        return attr
      })

      group.attributes = newAttributes
      return group
    })

    formikRef.current?.setFieldValue('attributes', newProductAttributes)
  }, 1000)

  const changeVariations = useCallback((variations) => {
    formikRef.current?.setFieldValue('variations', variations)
  }, [])

  const handleChangePriceVariations = useCallback((priceVariations) => {
    formikRef.current?.setFieldValue('priceVariations', priceVariations)
  }, [])

  return (
        <Formik
            initialValues={initValues}
            onSubmit={onSubmit}
            validationSchema={productValidationSchema}
            innerRef={(p) => {
              if (p) {
                formikRef.current = p
              }
            }}
        >
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              submitForm,
              setFieldValue
            }) => (
                <Grid container spacing={2}>
                    <Grid item xs={12} md={6}>
                        <TextField
                            id="product-title"
                            label="عنوان محصول"
                            variant="outlined"
                            name="title"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.title}
                            error={!!errors.title}
                            helperText={errors.title && touched.title ? errors.title : null}
                            fullWidth
                        />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <TextField
                            id="product-slug"
                            label="عنوان محصول به انگلیسی"
                            variant="outlined"
                            name="slug"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.slug}
                            error={!!errors.slug}
                            helperText={errors.slug && touched.slug ? errors.slug : null}
                            fullWidth
                        />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <TextField
                            id="product-price"
                            label="قیمت به ریال"
                            variant="outlined"
                            name="price"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.price}
                            error={!!errors.price}
                            helperText={errors.price && touched.price ? errors.price : null}
                            fullWidth
                        />
                        {values.price && <FormHelperText id="product-show-price">
                          {currencyService.formatIRR(values.price)}
                        </FormHelperText>}
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <TextField
                            id="product-dPrice"
                            label="قیمت ویژه به ریال"
                            variant="outlined"
                            name="discountedPrice"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.discountedPrice}
                            error={!!errors.discountedPrice}
                            helperText={errors.discountedPrice && touched.discountedPrice ? errors.discountedPrice : null}
                            fullWidth
                        />
                        {values.discountedPrice > 0 && <FormHelperText id="product-show-price">
                          {currencyService.formatIRR(values.discountedPrice)}
                        </FormHelperText>}
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <TextField
                            id="product-stock"
                            label="موجودی"
                            variant="outlined"
                            name="stock"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.stock}
                            error={!!errors.stock}
                            helperText={errors.stock && touched.stock ? errors.stock : null}
                            fullWidth
                        />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <FormControl variant="outlined" fullWidth>
                            <InputLabel htmlFor="outlined-category-native-simple">انتخاب دسته بندی</InputLabel>
                            <Select
                            native
                            value={values.category}
                                    onChange={(e) => {
                                      setFieldValue('category', e.target.value)

                                      onChangeCategory(e.currentTarget.value)
                                    }}
                            label="انتخاب دسته بندی"
                            inputProps={{
                              name: 'category',
                              id: 'outlined-category-native-simple'
                            }}
                            >
                            <option aria-label="None" value={undefined} />
                            {categories.map((category:CategoryItemType) => (<option key={category.id} value={category.id}>{category.title}</option>))}
                            </Select>
                            {errors.category && touched.category && (
                                <FormHelperText id="product-gallery-error" error>
                                    {errors.category}
                                </FormHelperText>
                            )}
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} md={12}>
                      <Section title="ویژگی های محصول">
                        <ProductAttribute attributes={attributes} onAttributeChange={handleAttributeChange}/>
                      </Section>
                    </Grid>
                    <Grid item xs={12} md={12}>
                      <Section title="متغیرهای محصول">
                        <ProductVariations onChangeVariations={changeVariations}/>
                      </Section>
                    </Grid>
                    {values.variations.length > 0 && (<Grid item xs={12} md={12}>
                      <Section title="متغیرهای قیمت">
                        <PriceVariations
                          variations={values.variations}
                          onChangePriceVariations={handleChangePriceVariations}
                        />
                      </Section>
                    </Grid>)}
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                        <ImageUpload
                          title="تصویر تکی"
                          btnTitle="انتخاب تصویر"
                          id="product-thumbnail"
                          name="thumbnail"
                          helperText="فرمت های مجاز jpg, png"
                          onChange={handleChangeThumbnail}
                        />
                    </Grid>
                    {thumbnail && (
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                        <PreviewImage
                            images={[thumbnail]}
                            error={!!errors?.thumbnail}
                            onDelete={() => {}}
                        />
                        </Grid>
                    )}
                    {errors.thumbnail && touched.thumbnail && (
                        <FormHelperText id="product-gallery-error" error>
                        {errors.thumbnail}
                        </FormHelperText>
                    )}
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                        <ImageUpload
                          title="تصاویر گالری"
                          btnTitle="انتخاب تصویر"
                          id="product-gallery"
                          name="gallery"
                          helperText="فرمت های مجاز jpg, png"
                          onChange={handleChangeGallery}
                          multiple
                        />
                    </Grid>
                    {galleryImages && (
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                        <PreviewImage
                            images={galleryImages}
                            error={!!errors.gallery}
                            onDelete={() => {}}
                        />
                        </Grid>
                    )}
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                        {errors.gallery && touched.gallery && (
                        <FormHelperText id="product-gallery-error" error>
                            {errors.gallery}
                        </FormHelperText>
                        )}
                    </Grid>
                    <Grid item xs={12} md={12}>
                        <Button variant="contained" color="default" startIcon={<Save />} onClick={submitForm}>ذخیره سازی</Button>
                    </Grid>
                </Grid>
            )}
        </Formik>
  )
}

export default Form
