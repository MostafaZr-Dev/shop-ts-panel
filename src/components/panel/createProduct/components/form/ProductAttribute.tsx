// eslint-disable-next-line no-use-before-define
import React from 'react'
import { Box, Typography, Divider, FormControl, TextField } from '@material-ui/core'
import { styled } from '@material-ui/core/styles'

import ProductAttributesType from 'contracts/ProductAttributesType'
import AttributeItemType from 'contracts/AttributeItemType'

const InfoText = styled(Typography)({
  color: '#fb9300'
})

const AttributeHeader = styled(Typography)(({ theme }) => ({
  color: '#fb9300',
  marginBottom: theme.spacing(1)
}))

const AttributeConatiner = styled(Box)(({ theme }) => ({
  width: '100%',
  paddingTop: theme.spacing(1),
  paddingBottom: theme.spacing(1)
}))

const StyledFormControl = styled(FormControl)(({ theme }) => ({
  margin: theme.spacing(1, 'auto')
}))

type ProductAttributeProps = {
    attributes: ProductAttributesType[];
    onAttributeChange: (e:React.ChangeEvent<HTMLInputElement>, attributeID: string) => void
}

function ProductAttribute ({ attributes, onAttributeChange } : ProductAttributeProps) {
  const renderAttributes = attributes.map((group:ProductAttributesType) => (<>
    <AttributeHeader>{group.title}</AttributeHeader>
    <Divider />
    <AttributeConatiner>
        {group.attributes.map((attr:AttributeItemType, i) => (
            <>
                <StyledFormControl fullWidth>
                    <TextField
                      variant='outlined'
                      label={attr.title}
                      onChange={(e:React.ChangeEvent<HTMLInputElement>) => {
                        onAttributeChange(e, attr.hash)
                      }}
                      fullWidth
                    />
                </StyledFormControl>
            </>
        ))}
    </AttributeConatiner>

  </>))

  return (
      <>
        {attributes.length === 0 && <InfoText>دسته بندی انتخاب نشده است. یک دسته بندی انتخاب کنید</InfoText>}
        {attributes.length > 0 && <> {renderAttributes} </>}
      </>
  )
}

export default ProductAttribute
