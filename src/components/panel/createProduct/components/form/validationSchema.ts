import * as Yup from 'yup'

const allowedTypes = ['image/jpeg', 'image/png']

const productSchema = Yup.object({
  title: Yup.string().required('فیلد الزامی است!'),
  slug: Yup.string().required('فیلد الزامی است!'),
  price: Yup.number().typeError('مقدار عددی وارد کنید!').required('فیلد الزامی است!'),
  discountedPrice: Yup.number().typeError('مقدار عددی وارد کنید!').required('فیلد الزامی است!')
    .test(
      'valid_value',
      'قیمت ویژه باید کمتر از قیمت اصلی باشد!',
      function (value: number | undefined) {
        if (!value) {
          return true
        }
        return value < this.parent.price
      }
    ),
  stock: Yup.number().typeError('مقدار عددی وارد کنید!').required('فیلد الزامی است!'),
  category: Yup.string().required('فیلد الزامی است!'),
  thumbnail: Yup.mixed<File>().required('فیلد الزامی است!')
    .test('file_type', 'پسوند فایل باید jpeg یا png باشد!', (value) => {
      if (!value) {
        return true
      }
      return allowedTypes.includes(value.type)
    }),
  gallery: Yup.mixed<FileList>().required('فیلد الزامی است!')
    .test('empty', 'حداکثر 5 تصویر', (values) => {
      if (!values) {
        return true
      }
      return values.length <= 5
    })
    .test('file_type', 'پسوند فایل ها باید jpeg یا png باشد!', (values) => {
      if (!values) {
        return true
      }

      const invalidTypes = Array.from(values).filter(
        (image) => !allowedTypes.includes(image.type)
      )

      return invalidTypes.length === 0
    })
})

export default productSchema
