// eslint-disable-next-line no-use-before-define
import React, { useState, useEffect, useCallback } from 'react'
import { Grid, Button, TextField } from '@material-ui/core'

import { VariationType } from 'contracts/VariationsType'
import VariantSelect from './VariantSelect'
import Modal from 'components/modal'

type PriceVariation = {
  [index:string]: string;
}

export type PriceVariationItem = {

  items: PriceVariation;
  price: number;

}

type PriceVariationsProps = {
  variations: VariationType[];
  onChangePriceVariations: (priceVariations: PriceVariationItem[]) => void
}

function PriceVariations ({ variations, onChangePriceVariations } : PriceVariationsProps) {
  const [priceVariation, setPriceVariation] = useState<PriceVariation>()
  const [priceVariations, setPriceVariations] = useState<PriceVariationItem[]>([])
  const [showPriceModal, setShowPriceModal] = useState<boolean>(false)
  const [priceVariantAmount, setPriceVariantAmount] = useState<number>(0)

  useEffect(() => {
    onChangePriceVariations(priceVariations)
  }, [priceVariations])

  const addPriceVariantItem = (name:string, value: string) => {
    setPriceVariation(prev => ({
      ...prev,
      [name]: value
    }))
  }

  const closePriceModal = useCallback(() => {
    setShowPriceModal(false)
  }, [])

  const openPriceModal = useCallback(() => {
    setShowPriceModal(true)
  }, [])

  const addPriceVariant = () => {
    setPriceVariations((prev) => ([
      ...prev,
      {
        items: priceVariation as PriceVariation,
        price: priceVariantAmount
      }
    ]))
    closePriceModal()
  }

  const changePriceVariation = useCallback((e:React.ChangeEvent<HTMLInputElement>) => {
    setPriceVariantAmount(e.target.value as unknown as number)
  }, [])

  return (
        <Grid container spacing={2}>
          {variations.map((variationItem) => (
            <Grid item key={variationItem.hash} xs={6} md={3}>
              <VariantSelect
                title={variationItem.title}
                type={variationItem.type}
                items={variationItem.items}
                name={variationItem.name}
                onItemChanged={addPriceVariantItem}
              />
            </Grid>
          ))}
          <Grid item xs={12} md={12}>
            <Button variant="contained" onClick={openPriceModal}>اضافه کردن متغیر قیمت</Button>
          </Grid>
          <Modal
            open={showPriceModal}
            title="قیمت متغیر محصول"
            onClose={closePriceModal}
            actions={<>
              <Button variant="contained" color="secondary" onClick={closePriceModal}>لغو</Button>
              <Button variant="contained" color="primary" onClick={addPriceVariant}>تایید</Button>
          </>}
          >
            <TextField
                id="priceVariant"
                name="priceVariant"
                variant="outlined"
                value={priceVariantAmount}
                label="عنوان متغیر محصول"
                onChange={changePriceVariation}
                fullWidth
            />
          </Modal>
        </Grid>
  )
}

export default PriceVariations
