// eslint-disable-next-line no-use-before-define
import React from 'react'
import { Select, FormControl, InputLabel } from '@material-ui/core'

import { VariationItemType } from 'contracts/VariationsType'

type VariantSelectProps = {
    title: string;
    type: string;
    name: string;
    items: VariationItemType[];
    onItemChanged: (type:string, value: string) => void
}

function VariantSelect ({ title, type, name, items, onItemChanged }: VariantSelectProps) {
  const handleChangeItem = (e:React.ChangeEvent<{ name?: string | undefined; value: unknown; }>) => {
    onItemChanged(name, e.target.value as string)
  }

  return (
    <FormControl variant="outlined" fullWidth>
      <InputLabel htmlFor="outlined-age-native-simple">{`انتخاب از ${title}`}</InputLabel>
      <Select
        native
        label={title}
        inputProps={{
          name: 'age',
          id: 'variant-select'
        }}
        onChange={handleChangeItem}
      >
        <option aria-label="None" value="" />
        {items.map((item, i) => (<option key={i} value={item.value}>{item.title}</option>))}
      </Select>
    </FormControl>
  )
}

export default VariantSelect
