// eslint-disable-next-line no-use-before-define
import React from 'react'
import {
  Box,
  Typography
} from '@material-ui/core'
import { styled } from '@material-ui/core/styles'

const Conatiner = styled(Box)(({ theme }) => ({
  width: '100%'
}))

const HeaderConatiner = styled(Box)(({ theme }) => ({
  width: '100%',
  paddingBottom: theme.spacing(1)
}))

const ContentConatiner = styled(Box)(({ theme }) => ({
  width: '100%',
  border: '2px solid #eee',
  padding: theme.spacing(2)
}))

const Header = styled(Typography)({
  color: 'rgba(0,0,0,0.54)'
})

type SectionProps = {
    title: string;
    children: React.ReactNode;
}

function Section ({ title, children } : SectionProps) {
  return (
        <Conatiner>
            <HeaderConatiner>
                <Header>{title}</Header>
            </HeaderConatiner>
            <ContentConatiner>
                {children}
            </ContentConatiner>
        </Conatiner>
  )
}

export default Section
