// eslint-disable-next-line no-use-before-define
import React, { useState, useEffect, useCallback } from 'react'
import { useHistory } from 'react-router-dom'

import Container from '../container'
import Form from './components/form'

import CategoryItemType from 'contracts/CategoryItemType'
import ProductAttributesType from 'contracts/ProductAttributesType'
import useGetAPI from 'hooks/useGetAPI'
import usePostAPI from 'hooks/usePostAPI'
import { LinearProgress } from 'components/progress'
import ProductFormValues from './ProductFormValues'
import Toast from 'components/toast'

function CreateProduct () {
  const [categoryID, setCategoryID] = useState<string | null>(null)
  const [categories, setCategories] = useState<CategoryItemType[]>([])
  const [productAttribute, setProductAttribute] = useState<ProductAttributesType[]>([])
  const [progress, setProgress] = useState<number | undefined>(undefined)

  const history = useHistory()

  const [categoriesResponse, getCategoriesAPI] = useGetAPI({
    url: '/api/v1/admin/categories',
    headers: {}
  })

  const [attributesResponse, getProductAttributesAPI] = useGetAPI({
    url: `/api/v1/admin/categories/${categoryID}/attributes`,
    headers: {}
  })

  const [createProductResponse, createProductAPI] = usePostAPI({
    url: '/api/v1/admin/products',
    configs: {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      onUploadProgress: (progressEvent: { loaded: number; total: number }) => {
        const percent = Math.round((progressEvent.loaded * 100) / progressEvent.total)

        setProgress(percent as number)
      }
    }
  })

  const { isLoading, data } = categoriesResponse
  const { isLoading: attrLoading, data: attributeData } = attributesResponse
  // eslint-disable-next-line no-unused-vars
  const { isLoading: createProductLoading, success: createProductSuccess, data: createProductData } = createProductResponse
  useEffect(() => {
    getCategoriesAPI<CategoryItemType[]>()
  }, [])

  useEffect(() => {
    if (data) {
      setCategories(data.categories)
    }
  }, [data])

  useEffect(() => {
    if (categoryID) {
      getProductAttributesAPI<ProductAttributesType[]>()
    } else {
      setProductAttribute([])
    }
  }, [categoryID])

  useEffect(() => {
    if (attributeData) {
      setProductAttribute(attributeData.attributes)
    }
  }, [attributeData])

  useEffect(() => {
    let timeout: ReturnType<typeof setTimeout>
    if (createProductSuccess) {
      timeout = setTimeout(() => {
        history.push('/dashboard/products')
      }, 2000)
    }

    return () => {
      clearTimeout(timeout)
    }
  }, [createProductSuccess])

  const setCurrentCategory = useCallback(
    (catID:string) => {
      setCategoryID(catID)
    },
    []
  )

  const createProduct = useCallback((values: ProductFormValues) => {
    const formData = new FormData()
    console.log({ values })
    formData.append('title', values.title)
    formData.append('price', `${values.price}`)
    formData.append('discountedPrice', `${values.discountedPrice}`)
    formData.append('stock', `${values.stock}`)
    formData.append('category', values.category as string)
    formData.append('thumbnail', values.thumbnail as Blob)
    formData.append('attributes', JSON.stringify(values.attributes))
    formData.append('variations', JSON.stringify(values.variations))
    formData.append('priceVariations', JSON.stringify(values.priceVariations))

    const gallery = values.gallery as FileList

    Array.from(gallery).forEach((file) => {
      formData.append('gallery', file as Blob)
    })

    createProductAPI(formData)
  }, [])

  return (
        <Container title="ایجاد محصول جدید">
          {isLoading && <LinearProgress />}
          {attrLoading && <LinearProgress />}
          {createProductLoading && <LinearProgress determinate value={progress}/>}
          <Toast open={createProductSuccess} type="success" message="محصول با موفقیت ایجاد شد" />
          <Form categories={categories} attributes={productAttribute} onSubmit={createProduct} onUpdateCategory={setCurrentCategory}/>
        </Container>
  )
}

export default CreateProduct
