// eslint-disable-next-line no-use-before-define
import React from 'react'
import { Typography } from '@material-ui/core'

import { StyledPaper } from './ContainerStyles'

type ContainerProps = {
    title: string;
    children: React.ReactNode
}

function Container ({ title, children }: ContainerProps) {
  return (
    <StyledPaper>
      <Typography className="title">{title}</Typography>
      {children}
    </StyledPaper>
  )
}

export default Container
