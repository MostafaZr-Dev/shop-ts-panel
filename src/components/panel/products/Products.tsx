// eslint-disable-next-line no-use-before-define
import React, { useEffect, useState, useCallback } from 'react'

import Container from '../container'
import ProductsList from './components/productsList'
import useGetAPI from 'hooks/useGetAPI'
import useGetQueryString from 'hooks/useGetQueryString'
import ProductType from 'contracts/ProductType'
import { TableSkeleton } from 'components/skeleton'
import PaginationType from 'contracts/PaginationType'
import Pagination from 'components/pagination'

function Products () {
  const [products, setProducts] = useState<ProductType[]>([])
  const [pagination, setPagination] = useState<PaginationType | null>(null)

  const [queryString, setQueryString] = useGetQueryString()

  const [productsResponse, getProductsFromAPI] = useGetAPI({
    url: `/api/v1/admin/products?${queryString.query}`,
    headers: {}
  })

  const { isLoading, data } = productsResponse

  useEffect(() => {
    getProductsFromAPI<{products: ProductType[], _metadata: PaginationType}>()
  }, [queryString])

  useEffect(() => {
    if (data) {
      setProducts(data.products)
      setPagination(data._metadata)
    }
  }, [data])

  const editProduct = useCallback(
    (productID: string) => {

    },
    []
  )

  const deleteProduct = useCallback(
    (productID: string) => {

    },
    []
  )

  const handlePagination = useCallback(
    (e:React.ChangeEvent<unknown>, page:number) => {
      setQueryString('page', `${page}`)
    },
    [])

  const { page } = queryString.params

  return (
    <Container title="لیست محصولات">
      {isLoading && <TableSkeleton />}
      {!isLoading && <Pagination count={pagination?.totalPages as number} currentPage={parseInt(page ? page as string : '1')} onChange={handlePagination}/>}
      {!isLoading && (
        <ProductsList
          products={products}
          onEditProduct={editProduct}
          onDeleteProduct={deleteProduct}
        />)
      }
      {!isLoading && <Pagination count={pagination?.totalPages as number} currentPage={parseInt(page ? page as string : '1')} onChange={handlePagination}/>}
    </Container>
  )
}

export default Products
