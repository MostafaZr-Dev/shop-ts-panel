// eslint-disable-next-line no-use-before-define
import React from 'react'
import { Chip } from '@material-ui/core'

import ProductStatusEnum from '../../ProductStatus'

type ProductStatusProps ={
    status: ProductStatusEnum
}

function ProductStatus ({ status }:ProductStatusProps) {
  return (
        <>
            {status === ProductStatusEnum.INIT && <Chip style={{ backgroundColor: '#ff9800' }} label="درحال آماده سازی" />}
            {status === ProductStatusEnum.INACTIVE && <Chip style={{ backgroundColor: '#f44336' }} label="غیرفعال" />}
            {status === ProductStatusEnum.PUBLISHED && <Chip style={{ backgroundColor: '#4caf50' }} label="فعال" />}
        </>
  )
}

export default ProductStatus
