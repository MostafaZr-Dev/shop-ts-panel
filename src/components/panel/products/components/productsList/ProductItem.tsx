// eslint-disable-next-line no-use-before-define
import React from 'react'
import { TableCell, TableRow, Avatar, IconButton, Tooltip } from '@material-ui/core'
import { CropOriginal, Delete } from '@material-ui/icons'

import ProductType from 'contracts/ProductType'
import CurrencyService from 'services/currencyService'
import LangService from 'services/langService'
import ProductStatus from '../productStatus'
import { EditIcon } from './ProductsListStyle'

type ProductItemProps = {
    product: ProductType;
    onDeleteItem: (e:React.MouseEvent) => void;
    onEditItem: (e:React.MouseEvent) => void;
}

function ProductItem ({ product, onDeleteItem, onEditItem }: ProductItemProps) {
  return (
    <TableRow>
        <TableCell align='center'>
            <Avatar variant="square" src={product.thumbnail} alt="product-thumbnail" >
                <CropOriginal />
            </Avatar>
        </TableCell>
        <TableCell align='center'>{product.title}</TableCell>
        <TableCell align='center'>{CurrencyService.formatIRR(product.price)}</TableCell>
        <TableCell align='center'>{LangService.toPersianNumber(product.stock)}</TableCell>
        <TableCell align='center'>{LangService.toPersianNumber(product.createdAt)}</TableCell>
        <TableCell align='center'>{LangService.toPersianNumber(product.updatedAt)}</TableCell>
        <TableCell align='center'>{<ProductStatus status={product.status} />}</TableCell>
        <TableCell align='center'>
          <Tooltip title='ویرایش'>
            <IconButton onClick={onEditItem}>
              <EditIcon />
            </IconButton>
          </Tooltip>
          <Tooltip title='حذف'>
            <IconButton onClick={onDeleteItem}>
              <Delete />
            </IconButton>
          </Tooltip>
        </TableCell>
    </TableRow>
  )
}

export default ProductItem
