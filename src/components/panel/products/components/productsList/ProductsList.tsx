// eslint-disable-next-line no-use-before-define
import React from 'react'
import { TableContainer, Table, TableCell, TableBody, TableRow, Paper } from '@material-ui/core'

import ProductType from 'contracts/ProductType'
import ProductItem from './ProductItem'

import { StyledHeader } from './ProductsListStyle'

type ProductListProps = {

    products:ProductType[];
    onDeleteProduct: (productID: string) => void;
    onEditProduct: (productID: string) => void;

}

function ProductsList ({ products, onDeleteProduct, onEditProduct }:ProductListProps) {
  const renderProducts = products.map((product) => (
        <ProductItem
            key={product.id}
            product={product}
            onDeleteItem={(e:React.MouseEvent) => {
              onDeleteProduct(product.id)
            }}
            onEditItem={(e:React.MouseEvent) => {
              onEditProduct(product.id)
            }}
        />
  ))

  return (
    <TableContainer component={Paper}>
        <Table aria-label="simple table">

            <StyledHeader>
                <TableRow>
                    <TableCell align="center">تصویر</TableCell>
                    <TableCell align="center">عنوان</TableCell>
                    <TableCell align="center">قیمت</TableCell>
                    <TableCell align="center">موجودی</TableCell>
                    <TableCell align="center">تاریخ ایجاد</TableCell>
                    <TableCell align="center">تاریخ بروزرسانی</TableCell>
                    <TableCell align="center">وضعیت</TableCell>
                    <TableCell align="center">عملیات</TableCell>
                </TableRow>
            </StyledHeader>
            <TableBody>
                {renderProducts}
            </TableBody>
        </Table>
    </TableContainer>
  )
}

export default ProductsList
