// eslint-disable-next-line no-use-before-define
import React, { useState, useEffect } from 'react'

import Container from '../container'
import useGetAPI from 'hooks/useGetAPI'
import { TableSkeleton } from 'components/skeleton'
import CategoryList from './components/categoryList'
import CategoryItemType from 'contracts/CategoryItemType'

function Categories () {
  const [categories, setCategories] = useState<CategoryItemType[]>([])

  const [categoriesResponse, getCategoriesAPI] = useGetAPI({
    url: '/api/v1/admin/categories',
    headers: {}
  })

  const { isLoading, data } = categoriesResponse

  useEffect(() => {
    getCategoriesAPI()
  }, [])

  useEffect(() => {
    if (data) {
      setCategories(data.categories)
    }
  }, [data])

  const editCategory = () => {}
  const deleteCategory = () => {}

  return (
        <Container title="لیست دسته بندی ها">
            {isLoading && <TableSkeleton />}
            {!isLoading && <CategoryList categories={categories} onEdit={editCategory} onDelete={deleteCategory} />}
        </Container>
  )
}

export default Categories
