// eslint-disable-next-line no-use-before-define
import React from 'react'
import {
  Paper,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Table,
  IconButton
} from '@material-ui/core'
import { Edit, Delete } from '@material-ui/icons'

import { StyledHeader, StyledTooltip } from './CategoryListStyles'
import CategoryItemType from 'contracts/CategoryItemType'

type CategoryListProps = {

    categories: CategoryItemType[];
    onEdit: Function;
    onDelete: Function;

}

function CategoryList ({ categories, onEdit, onDelete }: CategoryListProps) {
  return (
      <TableContainer component={Paper}>
        <Table aria-label="simple table">
          <StyledHeader>
            <TableRow>
              <TableCell align="center">عنوان</TableCell>
              <TableCell align="center">slug</TableCell>
              <TableCell align="center">عملیات</TableCell>
            </TableRow>
          </StyledHeader>
          <TableBody>
            {categories.length > 0 && (
              <>
                {categories.map((item, index) => (
                  <TableRow key={item.id}>
                    <TableCell align="center">{item.title}</TableCell>
                    <TableCell align="center">{item.slug}</TableCell>
                    <TableCell align="center">
                    <StyledTooltip
                      title="ویرایش دسته بندی"
                      aria-label="edit"
                      placement="top"
                    >
                      <IconButton
                        onClick={(e) => {
                          onEdit(e, item.id)
                        }}
                      >
                        <Edit />
                      </IconButton>
                    </StyledTooltip>
                    <StyledTooltip
                      title="حذف دسته بندی"
                      aria-label="delete"
                      placement="top"
                    >
                      <IconButton
                        onClick={(e) => {
                          onDelete(e, { id: item.id })
                        }}
                      >
                        <Delete />
                      </IconButton>
                    </StyledTooltip>
                  </TableCell>
                  </TableRow>
                ))}
              </>
            )}
            {categories.length === 0 && (
              <TableRow>
                <TableCell colSpan={3} align="center">
                  دسته بندی افزوده نشده است!
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainer>
  )
}

export default CategoryList
