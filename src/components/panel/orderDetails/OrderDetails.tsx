// eslint-disable-next-line no-use-before-define
import React, { useState, useEffect, useCallback } from 'react'
import { useParams } from 'react-router-dom'

import Conatiner from '../container'
import useGetAPI from 'hooks/useGetAPI'
import usePatchAPI from 'hooks/usePatchAPI'
import { LinearProgress } from 'components/progress'
import OrderType from 'contracts/OrderType'
import Header from './components/header'
import Order from './components/order'
import OrderStatus from 'contracts/OrderStatus'
import Toast from 'components/toast'

type OrderDetailsParams = {
  orderID : string;
}

function OrderDetails () {
  const [order, setOrder] = useState<OrderType | null>(null)
  const [selectedOrderStatus, setSelectedOrderStatus] = useState<OrderStatus | null>(order?.status!)

  const { orderID } = useParams<OrderDetailsParams>()

  const [orderResponse, getOrderFromAPI] = useGetAPI({
    url: `/api/v1/admin/orders/${orderID}`,
    headers: {}
  })

  const [orderStatusResponse, setOrderStatusAPI] = usePatchAPI({
    url: `/api/v1/admin/orders/${orderID}`,
    configs: {}
  })

  const { isLoading, success, data } = orderResponse
  // eslint-disable-next-line no-unused-vars
  const { isLoading: changeStatusLoading, success: changeStatusSuccess, error: statusChangeError, data: changeStatusData } = orderStatusResponse

  useEffect(() => {
    getOrderFromAPI<OrderType>()
  }, [orderID])

  useEffect(() => {
    if (success && data) {
      setOrder(data.order)
    }
  }, [success])

  const changeOrderStatus = useCallback(
    (orderStatus:OrderStatus) => {
      setSelectedOrderStatus(orderStatus)
    },
    []
  )

  const updateOrderStatus = useCallback(
    () => {
      if (!selectedOrderStatus) {
        return
      }

      if (parseInt(`${selectedOrderStatus}`) === order?.status) {
        return
      }

      setOrderStatusAPI({
        orderStatus: selectedOrderStatus
      })
    },
    [order, selectedOrderStatus]
  )

  return (
        <Conatiner title={'جزئیات سفارش'}>
              <Header orderStatus={order?.status} onUpdateStatus={updateOrderStatus} onChangeStatus={changeOrderStatus}/>
              <Toast open={changeStatusSuccess} type='success' message='وضعیت سفارش با موفقیت تغییر کرد' />
              <Toast open={statusChangeError} type='error' message={changeStatusData} />
              {(isLoading || changeStatusLoading) && <LinearProgress />}
              {!isLoading && <Order order={order as OrderType} />}
        </Conatiner>
  )
}

export default OrderDetails
