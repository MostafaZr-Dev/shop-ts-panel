import { Box } from '@material-ui/core'
import { styled } from '@material-ui/core/styles'

export const HeaderContainer = styled(Box)(({ theme }) => ({
  display: 'flex',
  width: '100%',
  margin: theme.spacing(3, 'auto'),
  justifyContent: 'space-between'
}))

export const StatusContainer = styled(Box)(({ theme }) => ({
  display: 'flex',
  minWidth: '200px',
  justifyContent: 'space-between',
  alignItems: 'center'
}))
