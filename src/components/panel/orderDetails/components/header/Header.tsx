// eslint-disable-next-line no-use-before-define
import React, { useState } from 'react'
import { FormControl, InputLabel, Select, Button } from '@material-ui/core'

import { HeaderContainer, StatusContainer } from './HeaderStyles'
import OrderStatus from 'contracts/OrderStatus'
import RouteButton from 'router/RouteButton'

type HeaderProps = {
    orderStatus: OrderStatus | undefined;
    onUpdateStatus: () => void;
    onChangeStatus: (orderStatus: OrderStatus)=> void
}

function Header ({ orderStatus, onUpdateStatus, onChangeStatus }: HeaderProps) {
  const [status, setStatus] = useState<OrderStatus | undefined>(orderStatus)

  const handleChangeStatus = (e:React.ChangeEvent<{value:unknown;} >) => {
    setStatus(e.target.value as OrderStatus)
    onChangeStatus(e.target.value as OrderStatus)
  }

  return (
        <HeaderContainer>
            <StatusContainer>
                <FormControl variant="outlined" style={{ width: '60%' }}>
                    <InputLabel htmlFor="order_status">تغییر وضعیت سفارش</InputLabel>
                    <Select
                        native
                        value={status || orderStatus}
                        onChange={handleChangeStatus}
                        label="تغییر وضعیت سفارش"
                        inputProps={{
                          name: 'orderStatus',
                          id: 'order_status'
                        }}
                        fullWidth
                    >
                        <option value={OrderStatus.INIT}>ثبت شده</option>
                        <option value={OrderStatus.PAID}>پرداخت شده</option>
                        <option value={OrderStatus.CONFIRMED}>تایید شده</option>
                        <option value={OrderStatus.INVENTORY}>درحال پردازش (انبار)</option>
                        <option value={OrderStatus.READY}>آماده ارسال</option>
                        <option value={OrderStatus.SENT}>ارسال شده</option>
                        <option value={OrderStatus.DELIVERED}>تحویل داده شده</option>
                        <option value={OrderStatus.CANCELED}>لغو شده</option>
                        <option value={OrderStatus.REFUNDED}>مرجوع شده</option>
                    </Select>
                </FormControl>
                <FormControl>
                    <Button
                        variant='contained'
                        color='primary'
                        onClick={onUpdateStatus}
                    >بروزرسانی</Button>
                </FormControl>
            </StatusContainer>
            <RouteButton to='/dashboard/orders' title='لیست سفارشات' />
        </HeaderContainer>
  )
}

export default Header
