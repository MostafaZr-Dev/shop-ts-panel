// eslint-disable-next-line no-use-before-define
import React, { useCallback } from 'react'
import {
  Grid,
  Typography,
  TableContainer,
  Table,
  TableRow,
  TableCell,
  TableBody
} from '@material-ui/core'

import OrderType, { OrderLineType } from 'contracts/OrderType'
import OrderSection from './OrderSection'
import { StyledHeader, StyledDivider } from './OrderStyles'

type OrderProps = {
    order: OrderType
}

function Order ({ order }: OrderProps) {
  const getFinalPrice = useCallback(
    (orderLine: OrderLineType): number => {
      if (orderLine.discountedPrice) {
        return orderLine.discountedPrice * orderLine.quantity
      }

      return orderLine.price * orderLine.quantity
    },
    []
  )

  const renderOrderLines = order?.orderLines.map((item, i) => <TableRow key={i}>
      <TableCell align="center">{item.product?.title}</TableCell>
      <TableCell align="center">{item.price}</TableCell>
      <TableCell align="center">{item.discountedPrice ? item.discountedPrice : '-'}</TableCell>
      <TableCell align="center">{item.quantity}</TableCell>
      <TableCell align="center">{getFinalPrice(item)}</TableCell>
    </TableRow>
  )

  return (
        <Grid container>
            <Grid item xs={12} md={12}>
                <OrderSection title='آدرس ارسال'>
                    ----
                </OrderSection>
            </Grid>
            <Grid item xs={12} md={12}>
                <OrderSection title='مشخصات مشتری'>
                    <Grid container direction='row' justify='center' spacing={3}>
                        <Grid item xs={12} md={4} >
                            <Typography>نام</Typography>
                            <StyledDivider />
                            <Typography>{order?.user.firstName}</Typography>
                        </Grid>
                        <Grid item xs={12} md={4}>
                            <Typography>نام خانوادگی</Typography>
                            <StyledDivider />
                            <Typography>{order?.user.lastName}</Typography>
                        </Grid>
                        <Grid item xs={12} md={4}>
                            <Typography>ایمیل</Typography>
                            <StyledDivider />
                            <Typography>{order?.user.email}</Typography>
                        </Grid>
                    </Grid>
                </OrderSection>
            </Grid>
            <Grid item xs={12} md={12}>
                <OrderSection title='اقلام سفارش'>
                    <TableContainer>
                        <Table aria-label="simple table">
                            <StyledHeader>
                                <TableRow>
                                    <TableCell align="center">عنوان محصول</TableCell>
                                    <TableCell align="center">قیمت</TableCell>
                                    <TableCell align="center">قیمت با تخفیف</TableCell>
                                    <TableCell align="center">تعداد</TableCell>
                                    <TableCell align="center">قیمت کل</TableCell>
                                </TableRow>
                            </StyledHeader>
                            <TableBody>
                                {renderOrderLines}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </OrderSection>
            </Grid>
        </Grid>
  )
}

export default Order
