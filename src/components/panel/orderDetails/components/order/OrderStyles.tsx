import { Typography, Box, TableHead, Divider } from '@material-ui/core'
import { styled } from '@material-ui/core/styles'

export const OrderSectionTitle = styled(Typography)(({ theme }) => ({
  padding: theme.spacing(1, 2),
  backgroundColor: '#fb3900',
  width: 'fit-content',
  color: '#fff',
  borderRadius: '5px 5px 0 0'
}))

export const OrderSectionContent = styled(Box)(({ theme }) => ({
  border: '2px solid #eee',
  padding: theme.spacing(3),
  marginBottom: theme.spacing(3)
}))

export const StyledDivider = styled(Divider)(({ theme }) => ({
  margin: theme.spacing(2, 'auto')
}))

export const StyledHeader = styled(TableHead)({
  backgroundColor: '#005a8d',
  color: '#fff',

  '& .MuiTableCell-head': {
    color: '#fff'
  }
})
