// eslint-disable-next-line no-use-before-define
import React from 'react'
import { Box } from '@material-ui/core'

import { OrderSectionTitle, OrderSectionContent } from './OrderStyles'

type OrderSectionProps = {
    title: string;
    children: React.ReactNode;
}

function OrderSection ({ title, children }: OrderSectionProps) {
  return (
        <Box>
            <OrderSectionTitle>{title}</OrderSectionTitle>
            <OrderSectionContent>
                {children}
            </OrderSectionContent>
        </Box>
  )
}

export default OrderSection
