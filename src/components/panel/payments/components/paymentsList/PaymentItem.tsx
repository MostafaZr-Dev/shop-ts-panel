// eslint-disable-next-line no-use-before-define
import React from 'react'
import { TableCell, TableRow } from '@material-ui/core'

import PaymentType from 'contracts/PaymentType'
import CurrencyService from 'services/currencyService'
import LangService from 'services/langService'
import PaymentStatus from './PaymentStatus'

type PaymentItemProps = {
    payment: PaymentType
}

function PaymentItem ({ payment }:PaymentItemProps) {
  return (
        <TableRow>
            <TableCell align="center">{`${payment.user.firstName} ${payment.user.lastName}`}</TableCell>
            <TableCell align="center">{payment.order.id}</TableCell>
            <TableCell align="center">{CurrencyService.formatIRR(payment.amount)}</TableCell>
            <TableCell align="center">{payment.method}</TableCell>
            <TableCell align="center">{LangService.toPersianNumber(payment.createdAt)}</TableCell>
            <TableCell align="center">{LangService.toPersianNumber(payment.updatedAt)}</TableCell>
            <TableCell align="center">
              <PaymentStatus status={payment.status}/>
            </TableCell>
        </TableRow>
  )
}

export default PaymentItem
