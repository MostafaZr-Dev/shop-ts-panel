// eslint-disable-next-line no-use-before-define
import React from 'react'
import { TableContainer, Table, TableRow, TableCell, TableBody, Paper } from '@material-ui/core'

import { StyledHeader } from './PaymentsListStyles'
import PaymentType from 'contracts/PaymentType'
import PaymentItem from './PaymentItem'

type PaymentListProps = {
    payments: PaymentType[];
}

function PaymentsList ({ payments }: PaymentListProps) {
  const renderPayments = payments.map((payment) => (
        <PaymentItem key={payment.id} payment={payment}/>
  ))

  return (
    <TableContainer component={Paper}>
        <Table aria-label="simple table">

            <StyledHeader>
                <TableRow>
                    <TableCell align="center">مشتری</TableCell>
                    <TableCell align="center">سفارش</TableCell>
                    <TableCell align="center">مبلغ</TableCell>
                    <TableCell align="center">روش پرداخت</TableCell>
                    <TableCell align="center">تاریخ ایجاد</TableCell>
                    <TableCell align="center">تاریخ بروزرسانی</TableCell>
                    <TableCell align="center">وضعیت</TableCell>
                </TableRow>
            </StyledHeader>
            <TableBody>
                {payments.length === 0 && (
                    <TableRow>
                        <TableCell align="center" colSpan={7}>پرداختی ثبت نشده است</TableCell>
                    </TableRow>
                )}
                {payments.length > 0 && (
                    <> {renderPayments} </>
                )}
            </TableBody>
        </Table>
    </TableContainer>
  )
}

export default PaymentsList
