// eslint-disable-next-line no-use-before-define
import React from 'react'
import { Chip } from '@material-ui/core'

import PaymentStatusEnum from 'contracts/PaymentStatus'

type PaymentStatusProps ={
    status: PaymentStatusEnum
}

function PaymentStatus ({ status }: PaymentStatusProps) {
  return (
        <>
            {status === PaymentStatusEnum.PENDING && <Chip style={{ backgroundColor: '#ff9800' }} label="درحال پرداخت" />}
            {status === PaymentStatusEnum.SUCCESS && <Chip style={{ backgroundColor: 'green' }} label="موفق" />}
            {status === PaymentStatusEnum.FAILED && <Chip style={{ backgroundColor: '#4caf50' }} label="ناموفق" />}
        </>
  )
}

export default PaymentStatus
