// eslint-disable-next-line no-use-before-define
import React, { useState, useEffect, useCallback } from 'react'

import Container from '../container'
import PaymentType from 'contracts/PaymentType'
import PaymentList from './components/paymentsList'
import useGetAPI from 'hooks/useGetAPI'
import useGetQueryString from 'hooks/useGetQueryString'
import PaginationType from 'contracts/PaginationType'
import Pagination from 'components/pagination'
import { TableSkeleton } from 'components/skeleton'

function Payments () {
  const [payments, setPayments] = useState<PaymentType[]>([])
  const [pagination, setPagination] = useState<PaginationType | null>(null)

  const [queryString, setQueryString] = useGetQueryString()

  const [paymentsResponse, getPaymentsFromAPI] = useGetAPI({
    url: '/api/v1/admin/payments',
    headers: {}
  })

  const { isLoading, success, data } = paymentsResponse

  useEffect(() => {
    getPaymentsFromAPI<{_metadata: PaginationType, payments:PaymentType[]}>()
  }, [])

  useEffect(() => {
    if (success && data) {
      setPayments(data.payments)
      setPagination(data._metadata)
    }
  }, [success])

  const handlePagination = useCallback(
    (e:React.ChangeEvent<unknown>, page:number) => {
      setQueryString('page', `${page}`)
    },
    [])

  const { page } = queryString.params

  return (
        <Container title='پرداخت ها'>
            {isLoading && <TableSkeleton />}
            {!isLoading && <PaymentList payments={payments}/>}
            {!isLoading && payments.length > 0 && <Pagination count={pagination?.totalPages as number} currentPage={parseInt(page ? page as string : '1')} onChange={handlePagination}/>}
        </Container>
  )
}

export default Payments
