// eslint-disable-next-line no-use-before-define
import React, { useState, useEffect } from 'react'
import { Grid } from '@material-ui/core'

import Container from '../container'
import RouteButton from 'router/RouteButton'
import useGetAPI from 'hooks/useGetAPI'
import { TableSkeleton } from 'components/skeleton'
import PaginationType from 'contracts/PaginationType'
import SettingType from 'contracts/SettingType'
import SettingsList from './components/settingsList'

function Settings () {
  const [settings, setSettings] = useState<SettingType[]>([])

  const [settingsResponse, getSettingsFromAPI] = useGetAPI({
    url: '/api/v1/settings',
    headers: {}
  })

  const { isLoading, success, data } = settingsResponse

  useEffect(() => {
    getSettingsFromAPI<{_metadata: PaginationType, orders:SettingType[]}>()
  }, [])

  useEffect(() => {
    if (success && data) {
      setSettings(data.settings)
    }
  }, [success])

  return (
        <Container title='تنظیمات'>
            <Grid container spacing={3}>
                <Grid item xs={12} md={12}>
                    <RouteButton to='/dashboard/settings/create' title='ایجاد تنظیمات' />
                </Grid>
                <Grid item xs={12} md={12}>
                    {isLoading && <TableSkeleton />}
                    {!isLoading && <SettingsList settings={settings} />}
                </Grid>
            </Grid>
        </Container>
  )
}

export default Settings
