// eslint-disable-next-line no-use-before-define
import React from 'react'
import { TableCell, TableRow } from '@material-ui/core'

import LangService from 'services/langService'
import SettingType from 'contracts/SettingType'
import SettingScope from './SettingScope'

type SettingItemProps = {
    setting: SettingType
}

function SettingItem ({ setting }: SettingItemProps) {
  return (
        <TableRow>
            <TableCell align='center'>{setting.title}</TableCell>
            <TableCell align='center'>{setting.key}</TableCell>
            <TableCell align='center'>{setting.value}</TableCell>
            <TableCell align='center'>{<SettingScope scope={setting.scope} />}</TableCell>
            <TableCell align='center'>{LangService.toPersianNumber(setting.version)}</TableCell>
            <TableCell align='center'>

            </TableCell>
        </TableRow>
  )
}

export default SettingItem
