// eslint-disable-next-line no-use-before-define
import React from 'react'
import { Chip } from '@material-ui/core'

import SettingScopeEnum from 'contracts/SettingsScope'

type SettingScopeProps = {
    scope: SettingScopeEnum
}

function SettingScope ({ scope }: SettingScopeProps) {
  return (
        <>
        { scope === SettingScopeEnum.PUBLIC && <Chip style= {{ backgroundColor: '#4caf50' }} label = "عمومی" />}
        { scope === SettingScopeEnum.PRIVATE && <Chip style={ { backgroundColor: '#f44336' } } label = "خصوصی" />}
      </>
  )
}

export default SettingScope
