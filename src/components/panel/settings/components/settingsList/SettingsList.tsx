// eslint-disable-next-line no-use-before-define
import React from 'react'
import { TableContainer, Table, TableRow, TableCell, TableBody, Paper } from '@material-ui/core'

import { StyledHeader } from './SettingsListStyles'
import SettingType from 'contracts/SettingType'
import SettingItem from './SettingItem'

type SettingsListProps = {
    settings: SettingType[];
}

function SettingsList ({ settings }: SettingsListProps) {
  const renderSettings = settings.map(setting => <SettingItem key={setting.id} setting={setting} />)
  return (
        <TableContainer component={Paper}>
        <Table aria-label="simple table">

            <StyledHeader>
                <TableRow>
                    <TableCell align="center">عنوان</TableCell>
                    <TableCell align="center">کلید</TableCell>
                    <TableCell align="center">مقدار</TableCell>
                    <TableCell align="center">نوع</TableCell>
                    <TableCell align="center">نسخه</TableCell>
                    <TableCell align="center">عملیات</TableCell>
                </TableRow>
            </StyledHeader>
            <TableBody>
                {renderSettings}
            </TableBody>
        </Table>
    </TableContainer>
  )
}

export default SettingsList
