// eslint-disable-next-line no-use-before-define
import React, { useState, useEffect } from 'react'
import { Grid } from '@material-ui/core'

import CouponType from 'contracts/CouponType'
import Container from '../container'
import RouteButton from 'router/RouteButton'
import CouponsList from './components/couponsList'
import useGetAPI from 'hooks/useGetAPI'
import { TableSkeleton } from 'components/skeleton'

function Coupons () {
  const [coupons, setCoupons] = useState<CouponType[]>([])

  const [couponsResponse, getCouponsFromAPI] = useGetAPI({
    url: '/api/v1/admin/coupons',
    headers: {}
  })

  const { isLoading, success, data } = couponsResponse

  useEffect(() => {
    getCouponsFromAPI<CouponType[]>()
  }, [])

  useEffect(() => {
    if (success && data) {
      setCoupons(data.coupons)
    }
  }, [success])

  return (
        <Container title='کدهای تخفیف'>
          <Grid container spacing={3}>
            <Grid item xs={12} md={12}>
              <RouteButton to='/dashboard/financial/coupons/create' title='ایجاد کد تخفیف' />
            </Grid>
            <Grid item xs={12} md={12}>
              {isLoading && <TableSkeleton />}
              {!isLoading && <CouponsList coupons={coupons} />}
            </Grid>
          </Grid>
        </Container>
  )
}

export default Coupons
