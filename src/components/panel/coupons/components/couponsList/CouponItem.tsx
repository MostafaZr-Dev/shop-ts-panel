// eslint-disable-next-line no-use-before-define
import React from 'react'
import { TableCell, TableRow } from '@material-ui/core'

import CouponType from 'contracts/CouponType'
import LangService from 'services/langService'
import CouponStatus from './CouponStatus'

type CouponItemProps = {
    coupon: CouponType;
}

function CouponItem ({ coupon }:CouponItemProps) {
  return (
    <TableRow>
        <TableCell align='center'>{coupon.code}</TableCell>
        <TableCell align='center'>{LangService.toPersianNumber(coupon.percent)}</TableCell>
        <TableCell align='center'>{LangService.toPersianNumber(coupon.limit)}</TableCell>
        <TableCell align='center'>{LangService.toPersianNumber(coupon.expiresAt ? coupon.expiresAt : '-')}</TableCell>
        <TableCell align='center'>{<CouponStatus status={coupon.status} />}</TableCell>
        <TableCell align='center'>

        </TableCell>
    </TableRow>
  )
}

export default CouponItem
