// eslint-disable-next-line no-use-before-define
import React from 'react'
import { TableContainer, Table, TableRow, TableCell, TableBody, Paper } from '@material-ui/core'

import { StyledHeader } from './CouponsListStyles'
import CouponType from 'contracts/CouponType'
import CouponItem from './CouponItem'

type CouponsListProps = {
    coupons: CouponType[]
}

function CouponsList ({ coupons }: CouponsListProps) {
  const renderCoupons = coupons.map(coupon => (
        <CouponItem key={coupon.id} coupon={coupon} />
  ))

  return (
    <TableContainer component={Paper}>
        <Table aria-label="simple table">

            <StyledHeader>
                <TableRow>
                    <TableCell align="center">کد</TableCell>
                    <TableCell align="center">درصد</TableCell>
                    <TableCell align="center">محدودیت</TableCell>
                    <TableCell align="center">انقضاء</TableCell>
                    <TableCell align="center">وضعیت</TableCell>
                    <TableCell align="center">عملیات</TableCell>
                </TableRow>
            </StyledHeader>
            <TableBody>
                {renderCoupons}
            </TableBody>
        </Table>
    </TableContainer>
  )
}

export default CouponsList
