// eslint-disable-next-line no-use-before-define
import React from 'react'
import { Chip } from '@material-ui/core'

import CouponStatusEnum from 'contracts/CouponStatus'

type CouponStatusProps ={
    status: CouponStatusEnum
}

function CouponStatus ({ status } : CouponStatusProps) {
  return (
        <>
            {status === CouponStatusEnum.ACTIVE && <Chip style={{ backgroundColor: '#4caf50' }} label="فعال" />}
            {status === CouponStatusEnum.INACTIVE && <Chip style={{ backgroundColor: '#f44336' }} label="غیرفعال" />}
        </>
  )
}

export default CouponStatus
