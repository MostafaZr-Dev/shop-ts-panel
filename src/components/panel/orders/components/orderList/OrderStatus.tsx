// eslint-disable-next-line no-use-before-define
import React from 'react'
import { Chip } from '@material-ui/core'

import OrderStatusEnum from '../../OrderStatus'

type OrderStatusProps ={
    status: OrderStatusEnum
}

function OrderStatus ({ status } : OrderStatusProps) {
  return (
        <>
            {status === OrderStatusEnum.INIT && <Chip style={{ backgroundColor: '#ff9800' }} label="ثبت شده" />}
            {status === OrderStatusEnum.PAID && <Chip style={{ backgroundColor: '#4caf50' }} label="پرداخت شده" />}
            {status === OrderStatusEnum.CONFIRMED && <Chip style={{ backgroundColor: '#4caf50' }} label="تایید شده" />}
            {status === OrderStatusEnum.INVENTORY && <Chip style={{ backgroundColor: '#4caf50' }} label="در حال پردازش (انبار)" />}
            {status === OrderStatusEnum.READY && <Chip style={{ backgroundColor: '#4caf50' }} label="آماده ارسال" />}
            {status === OrderStatusEnum.SENT && <Chip style={{ backgroundColor: '#4caf50' }} label="ارسال شده" />}
            {status === OrderStatusEnum.DELIVERED && <Chip style={{ backgroundColor: '#4caf50' }} label="تحویل داده شده" />}
            {status === OrderStatusEnum.CANCELED && <Chip style={{ backgroundColor: '#f44336' }} label="لغو شده" />}
            {status === OrderStatusEnum.REFUNDED && <Chip style={{ backgroundColor: '#ff9800' }} label="مرجوع شده" />}
        </>
  )
}

export default OrderStatus
