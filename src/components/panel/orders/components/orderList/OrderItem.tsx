/* eslint-disable no-use-before-define */
import React from 'react'
import { TableCell, TableRow, IconButton, Tooltip } from '@material-ui/core'
import { Delete, Visibility } from '@material-ui/icons'
import { Link } from 'react-router-dom'

import OrderType from 'contracts/OrderType'
import { EditIcon } from './OrdersListStyles'
import CurrencyService from 'services/currencyService'
import LangService from 'services/langService'
import OrderStatus from './OrderStatus'

type OrderItemProps = {
    order: OrderType;
    onDeleteItem: (e:React.MouseEvent) => void;
    onEditItem: (e:React.MouseEvent) => void;
}

function OrderItem ({ order, onDeleteItem, onEditItem }:OrderItemProps) {
  return (
        <TableRow>
          <TableCell align='center'>{`${order.user.firstName} ${order.user.lastName}`}</TableCell>
          <TableCell align='center'>{CurrencyService.formatIRR(order.finalPrice)}</TableCell>
          <TableCell align='center'>{LangService.toPersianNumber(order.orderLines.length)}</TableCell>
          <TableCell align='center'>{LangService.toPersianNumber(order.createdAt)}</TableCell>
          <TableCell align='center'>{LangService.toPersianNumber(order.updatedAt)}</TableCell>
          <TableCell align='center'>{<OrderStatus status={order.status} />}</TableCell>
          <TableCell align='center'>
            <Tooltip title='جزئیات' placement='top'>
              <Link to={`/dashboard/orders/${order.id}`}>
                <IconButton>
                  <Visibility color='primary' />
                </IconButton>
              </Link>
            </Tooltip>
            <Tooltip title='ویرایش' placement='top'>
              <IconButton onClick={onEditItem}>
                <EditIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title='حذف' placement='top'>
              <IconButton onClick={onDeleteItem}>
                <Delete />
              </IconButton>
            </Tooltip>
          </TableCell>
    </TableRow>
  )
}

export default OrderItem
