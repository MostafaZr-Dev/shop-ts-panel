// eslint-disable-next-line no-use-before-define
import React from 'react'
import { TableContainer, Table, TableRow, TableCell, TableBody, Paper } from '@material-ui/core'

import OrderType from 'contracts/OrderType'
import { StyledHeader } from './OrdersListStyles'
import OrderItem from './OrderItem'

type OrdersListProps = {
    orders: OrderType[];
    onDeleteOrder: (orderID: string) => void;
    onEditOrder: (orderID: string) => void;
}

function OrdersList ({ orders, onDeleteOrder, onEditOrder }: OrdersListProps) {
  const renderOrders = orders.map((order) => (
    <OrderItem
        key={order.id}
        order={order}
        onDeleteItem={(e:React.MouseEvent) => {
          onDeleteOrder(order.id)
        }}
        onEditItem={(e:React.MouseEvent) => {
          onEditOrder(order.id)
        }}
    />
  ))

  return (
    <TableContainer component={Paper}>
      <Table aria-label="simple table">

          <StyledHeader>
              <TableRow>
                  <TableCell align="center">مشتری</TableCell>
                  <TableCell align="center">قیمت</TableCell>
                  <TableCell align="center">تعداد آیتم ها</TableCell>
                  <TableCell align="center">تاریخ ایجاد</TableCell>
                  <TableCell align="center">تاریخ بروزرسانی</TableCell>
                  <TableCell align="center">وضعیت</TableCell>
                  <TableCell align="center">عملیات</TableCell>
              </TableRow>
          </StyledHeader>
          <TableBody>
              {renderOrders}
          </TableBody>
      </Table>
    </TableContainer>
  )
}

export default OrdersList
