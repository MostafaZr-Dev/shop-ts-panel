import { TableHead } from '@material-ui/core'
import { styled } from '@material-ui/core/styles'
import { Edit } from '@material-ui/icons'

export const StyledHeader = styled(TableHead)({
  backgroundColor: '#005a8d',
  color: '#fff',

  '& .MuiTableCell-head': {
    color: '#fff'
  }
})

export const EditIcon = styled(Edit)({
  color: '#ff3900'
})
