// eslint-disable-next-line no-use-before-define
import React, { useState, useEffect, useCallback } from 'react'

import Container from '../container'
import OrdersList from './components/orderList'
import useGetAPI from 'hooks/useGetAPI'
import useGetQueryString from 'hooks/useGetQueryString'
import OrderType from 'contracts/OrderType'
import { TableSkeleton } from 'components/skeleton'
import PaginationType from 'contracts/PaginationType'
import Pagination from 'components/pagination'
import Search from 'components/search'

function Orders () {
  const [orders, setOrders] = useState<OrderType[]>([])
  const [pagination, setPagination] = useState<PaginationType | null>(null)

  const [queryString, setQueryString] = useGetQueryString()

  const [ordersResponse, getOrdersFromAPI] = useGetAPI({
    url: `/api/v1/admin/orders?${queryString.query}`,
    headers: {}
  })

  const { isLoading, success, data } = ordersResponse

  useEffect(() => {
    getOrdersFromAPI<{_metadata: PaginationType, orders:OrderType[]}>()
  }, [queryString])

  useEffect(() => {
    if (success && data) {
      setOrders(data.orders)
      setPagination(data._metadata)
    }
  }, [success])

  const editOrder = (orderID:string) => {}
  const deleteOrder = (orderID:string) => {}

  const handlePagination = useCallback(
    (e:React.ChangeEvent<unknown>, page:number) => {
      setQueryString('page', `${page}`)
    },
    [])

  const searchOrders = useCallback(
    (keyword:string) => {
      setQueryString('q', keyword, 'page')
    },
    []
  )

  const { q: keyword, page } = queryString.params

  return (
        <Container title="لیست سفارشات">
            {isLoading && <TableSkeleton />}
            {!isLoading && <Search label='جستجو بر اساس نام | ایمیل | شماره سفارش' defaultValue={keyword as string} onChange={searchOrders} />}
            {!isLoading && <OrdersList orders={orders} onEditOrder={editOrder} onDeleteOrder={deleteOrder}/>}
            {!isLoading && <Pagination count={pagination?.totalPages as number} currentPage={parseInt(page ? page as string : '1')} onChange={handlePagination}/>}
        </Container>
  )
}

export default Orders
