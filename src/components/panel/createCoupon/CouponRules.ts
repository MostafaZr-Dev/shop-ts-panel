
export default interface CouponRules {

    user: string;
    maxPrice: number;
    minPrice: number;
    firstOrder: boolean;

}
