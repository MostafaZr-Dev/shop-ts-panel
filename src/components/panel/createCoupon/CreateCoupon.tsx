// eslint-disable-next-line no-use-before-define
import React, { useEffect, useCallback } from 'react'
import { useHistory } from 'react-router-dom'

import Container from '../container'
import Form from './components/form'
import CouponFormValues from './CouponFormValues'
import usePostAPI from 'hooks/usePostAPI'
import { LinearProgress } from 'components/progress'
import Toast from 'components/toast'

function CreateCoupon () {
  const [createCouponResponse, createCouponAPI] = usePostAPI({
    url: '/api/v1/admin/coupons'
  })

  const history = useHistory()

  const { isLoading, success } = createCouponResponse

  useEffect(() => {
    let timeout: ReturnType<typeof setTimeout>

    if (success) {
      timeout = setTimeout(() => {
        history.push('/dashboard/financial/coupons')
      }, 2000)
    }

    return () => {
      clearTimeout(timeout)
    }
  }, [success])

  const saveCoupon = useCallback(
    (values: CouponFormValues) => {
      const couponData = {
        code: values.code,
        percent: values.percent,
        limit: values.limit,
        expiresAt: values.expiresAt,
        constraints: {
          user: values.user,
          minPrice: values.minPrice,
          maxPrice: values.maxPrice,
          firstPurchase: values.firstOrder
        }
      }

      createCouponAPI(couponData)
    },
    []
  )

  return (
        <Container title='ایجاد کد تخفیف'>
            <Toast open={success} type='success' message='کوپن با موفقیت ایجاد شد' />
            {isLoading && <LinearProgress />}
            <Form onSubmit={saveCoupon} />
        </Container>
  )
}

export default CreateCoupon
