
export default interface CouponFormValues {

    code: string;
    percent: number | undefined;
    limit: number;
    expiresAt: string;
    user: string;
    minPrice: number;
    maxPrice: number;
    firstOrder: boolean;

}
