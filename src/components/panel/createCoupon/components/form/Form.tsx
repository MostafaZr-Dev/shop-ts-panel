// eslint-disable-next-line no-use-before-define
import React from 'react'
import {
  Grid,
  TextField,
  Button,
  FormHelperText,
  InputAdornment,
  FormControlLabel,
  Checkbox
} from '@material-ui/core'
import { Formik, FormikHelpers } from 'formik'
import { Save, CalendarToday, Person } from '@material-ui/icons'

// eslint-disable-next-line no-unused-vars
import couponValidationSchema from './FormSchema'
import CouponFormValues from '../../CouponFormValues'
import FormSection from './FormSection'

type FormProps = {
    onSubmit: (values:CouponFormValues, formikHelpers:FormikHelpers<CouponFormValues>) => void | Promise<any>;
}

function Form ({ onSubmit }: FormProps) {
  const initValues : CouponFormValues = {
    code: '',
    percent: undefined,
    limit: 0,
    expiresAt: '',
    user: '',
    minPrice: 0,
    maxPrice: 0,
    firstOrder: false
  }

  return (
    <Formik
      initialValues={initValues}
      onSubmit={onSubmit}
      validationSchema={couponValidationSchema}
    >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          submitForm
        }) => (
            <Grid container spacing={2}>
                <Grid item xs={12} md={6}>
                    <TextField
                        id="coupon-code"
                        label="کد تخفیف"
                        variant="outlined"
                        name="code"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.code}
                        error={!!errors.code}
                        helperText={errors.code && touched.code ? errors.code : null}
                        fullWidth
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        id="coupon-percent"
                        label="درصد تخفیف"
                        variant="outlined"
                        name="percent"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.percent}
                        error={!!errors.percent}
                        helperText={errors.percent && touched.percent ? errors.percent : null}
                        fullWidth
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        id="coupon-limit"
                        label="تعداد"
                        variant="outlined"
                        name="limit"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.limit}
                        error={!!errors.limit}
                        helperText={errors.limit && touched.limit ? errors.limit : null}
                        fullWidth
                    />
                    <FormHelperText id="coupon-count-alert">
                          0 به معنای نامحدود می باشد
                    </FormHelperText>
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        id="coupon-expiresAt"
                        label="تاریخ انقضاء"
                        variant="outlined"
                        name="expiresAt"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.expiresAt}
                        error={!!errors.expiresAt}
                        helperText={errors.expiresAt && touched.expiresAt ? errors.expiresAt : null}
                        InputProps={{
                          startAdornment: (
                            <InputAdornment position="start">
                              <CalendarToday />
                            </InputAdornment>
                          ),
                          endAdornment: (
                            <InputAdornment position="end">
                              yyyy/mm/dd
                            </InputAdornment>
                          )
                        }}
                        fullWidth
                    />
                    <FormHelperText id="coupon-count-alert">
                         عدم ثبت تاریخ یعنی کد هرگز منقضی نمی شود
                    </FormHelperText>
                </Grid>
                <Grid item xs={12} md={12}>
                    <FormSection title='شرایط استفاده'>
                        <Grid container spacing={2} alignItems='center'>
                          <Grid item xs={12} md={6}>
                            <TextField
                              id="coupon-user"
                              label="کاربر"
                              variant="outlined"
                              name="user"
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.user}
                              error={!!errors.user}
                              helperText={errors.user && touched.user ? errors.user : null}
                              InputProps={{
                                startAdornment: (
                                  <InputAdornment position="start">
                                    <Person />
                                  </InputAdornment>
                                )
                              }}
                            fullWidth
                            />
                            <FormHelperText id="coupon-count-alert">
                              فقط این کاربر می تواند از کد تخفیف استفاده کند
                            </FormHelperText>
                          </Grid>
                          <Grid item xs={12} md={6}>
                            <TextField
                                id="coupon-minPrice"
                                label="حداقل قیمت سفارش"
                                variant="outlined"
                                name="minPrice"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.minPrice}
                                error={!!errors.minPrice}
                                helperText={errors.minPrice && touched.minPrice ? errors.minPrice : null}
                              fullWidth
                              />
                              <FormHelperText id="coupon-count-alert">
                                زمانی قابل استفاده است که قیمت نهایی سفارش از این مقدار بیشتر باشد
                              </FormHelperText>
                          </Grid>
                          <Grid item xs={12} md={6}>
                            <TextField
                                id="coupon-maxPrice"
                                label="حداکثر قیمت سفارش"
                                variant="outlined"
                                name="maxPrice"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.maxPrice}
                                error={!!errors.maxPrice}
                                helperText={errors.maxPrice && touched.maxPrice ? errors.maxPrice : null}
                              fullWidth
                              />
                              <FormHelperText id="coupon-count-alert">
                                زمانی قابل استفاده است که قیمت نهایی سفارش کمتر از این مقدار باشد
                              </FormHelperText>
                          </Grid>
                          <Grid item xs={12} md={6}>
                            <FormControlLabel
                              control={
                                <Checkbox
                                  onChange={handleChange}
                                  name="firstOrder"
                                  color="primary"
                                />
                              }
                              label="فقط برای خرید بار اول"
                            />
                          </Grid>
                        </Grid>
                    </FormSection>
                </Grid>
                <Grid item xs={12} md={12}>
                  <Button variant="contained" color="default" startIcon={<Save />} onClick={submitForm}>ثبت کد تخفیف</Button>
                </Grid>
            </Grid>
        )}
    </Formik>
  )
}

export default Form
