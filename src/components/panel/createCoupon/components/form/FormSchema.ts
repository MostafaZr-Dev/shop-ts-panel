import * as Yup from 'yup'

const couponValidationSchema = Yup.object({
  code: Yup.string().required('فیلد الزامی است!'),
  percent: Yup.number().typeError('مقدار عددی وارد کنید!').required('فیلد الزامی است!'),
  limit: Yup.number().typeError('مقدار عددی وارد کنید!').required('فیلد الزامی است!')
})

export default couponValidationSchema
