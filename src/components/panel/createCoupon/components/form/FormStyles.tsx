import { Typography, Box } from '@material-ui/core'
import { styled } from '@material-ui/core/styles'

export const FormSectionTitle = styled(Typography)(({ theme }) => ({
  padding: theme.spacing(1, 2),
  backgroundColor: '#fb3900',
  width: 'fit-content',
  color: '#fff',
  borderRadius: '5px 5px 0 0'
}))

export const FormSectionContent = styled(Box)(({ theme }) => ({
  border: '2px solid #eee',
  padding: theme.spacing(3),
  marginBottom: theme.spacing(3)
}))
