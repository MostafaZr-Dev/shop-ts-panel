// eslint-disable-next-line no-use-before-define
import React from 'react'
import { Box } from '@material-ui/core'

import { FormSectionTitle, FormSectionContent } from './FormStyles'

type FormSectionProps = {
    title: string;
    children: React.ReactNode;
}

function FormSection ({ title, children }: FormSectionProps) {
  return (
        <Box>
            <FormSectionTitle>{title}</FormSectionTitle>
            <FormSectionContent>
                {children}
            </FormSectionContent>
        </Box>
  )
}

export default FormSection
