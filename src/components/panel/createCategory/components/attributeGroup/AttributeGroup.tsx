// eslint-disable-next-line no-use-before-define
import React from 'react'
import { Box, Divider, FormControl, IconButton } from '@material-ui/core'
import { AddBox, Delete } from '@material-ui/icons'

import { GroupTitleContainer, GroupTitle, AttributeAddButton, GroupDivider } from './AttributeGroupStyles'
import AttributeItemType from '../attribute/AttributeItemType'
import Attribute from '../attribute'

type AttributeGroupProps = {
    title: string;
    attributes: AttributeItemType[];
    onAddAttribute: React.MouseEventHandler<HTMLButtonElement>;
    onDeleteAttributeGroup: React.MouseEventHandler<HTMLButtonElement>;
    onUpdateAttribute:(attrHash:string, field: string, value: string | boolean) => void
}

function AttributeGroup ({ title, attributes, onAddAttribute, onDeleteAttributeGroup, onUpdateAttribute }: AttributeGroupProps) {
  const renderAttributes = attributes.map((attribute:AttributeItemType, i) => (<Attribute key={attribute.hash} {...attribute} onUpdateAttribute={onUpdateAttribute} />))

  return (
        <Box>
            <GroupTitleContainer>
                <GroupTitle variant="h6">{title}</GroupTitle>
                <IconButton onClick={onDeleteAttributeGroup}>
                    <Delete color="secondary"/>
                </IconButton>
            </GroupTitleContainer>
            <Divider />
            {renderAttributes}
            <FormControl>
                <AttributeAddButton
                    variant="contained"
                    startIcon={<AddBox />}
                    onClick={onAddAttribute}
                >
                    اضافه کردن ویژگی جدید
                </AttributeAddButton>
            </FormControl>
            <GroupDivider variant="middle"/>
        </Box>
  )
}

export default AttributeGroup
