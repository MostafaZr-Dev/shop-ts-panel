import AttributeItemType from '../attribute/AttributeItemType'

export default interface AttributeGroupType {

    hash: string;
    title: string;
    attributes: AttributeItemType[];

}
