import { Box, Button, Divider, Typography } from '@material-ui/core'
import { styled } from '@material-ui/core/styles'

export const GroupTitle = styled(Typography)(({ theme }) => ({
  fontSize: '1rem'
}))

export const GroupTitleContainer = styled(Box)(({ theme }) => ({
  marginBottom: theme.spacing(0.5),
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center'
}))

export const AttributeAddButton = styled(Button)(({ theme }) => ({
  backgroundColor: '#005a8d',
  color: '#fff',
  marginTop: theme.spacing(2),

  '&:hover': {
    backgroundColor: '#3edbf0'
  }
}))

export const GroupDivider = styled(Divider)(({ theme }) => ({
  margin: theme.spacing(3, 'auto'),
  backgroundColor: '#3edbf0'
}))
