// eslint-disable-next-line no-use-before-define
import React from 'react'
import { Grid, TextField, Switch, FormControlLabel } from '@material-ui/core'

import { AttributeContainer } from './AttributeStyles'
import AttributeItemType from './AttributeItemType'

type AttributeProps = AttributeItemType & {
    onUpdateAttribute:(attrHash:string, field: string, value: string | boolean) => void
}

function Attribute ({ hash, title, slug, filterable, hasPrice, onUpdateAttribute } : AttributeProps) {
  return (
        <AttributeContainer container spacing={2}>
            <Grid item xs={12} md={3}>
            <TextField
                variant="outlined"
                label="عنوان فارسی"
                id="attribute_title_fa"
                name="title"
                defaultValue={title}
                onBlur={
                    (e:React.FocusEvent<HTMLInputElement>) => {
                      onUpdateAttribute(hash, e.currentTarget.name, e.currentTarget.value)
                    } }
                fullWidth
            />
            </Grid>
            <Grid item xs={12} md={3}>
            <TextField
                variant="outlined"
                label="عنوان انگلیسی"
                id="attribute_title_en"
                name="slug"
                defaultValue={slug}
                onBlur={
                    (e:React.FocusEvent<HTMLInputElement>) => {
                      onUpdateAttribute(hash, e.currentTarget.name, e.currentTarget.value)
                    } }
                fullWidth
            />
            </Grid>
            <Grid item xs={12} md={3}>
            <FormControlLabel
                id="attribute_filterable"
                control={
                    <Switch
                    id="filterable"
                    name="filterable"
                    defaultChecked={filterable}
                    onChange={
                        (e:React.ChangeEvent<HTMLInputElement>) => {
                          onUpdateAttribute(hash, e.currentTarget.name, e.currentTarget.checked)
                        }}
                    />
                    }
                    label="استفاده در فیلترها"
            />
            </Grid>
            <Grid item xs={12} md={3}>
            <FormControlLabel
                id="attribute_price"
                control={
                    <Switch
                        id="hasPrice"
                        name="hasPrice"
                        onChange={
                            (e:React.ChangeEvent<HTMLInputElement>) => {
                              onUpdateAttribute(hash, e.currentTarget.name, e.currentTarget.checked)
                            }}
                    />
                }
                defaultChecked={hasPrice}
                label="استفاده در قیمت"
            />
            </Grid>
        </AttributeContainer>
  )
}

export default Attribute
