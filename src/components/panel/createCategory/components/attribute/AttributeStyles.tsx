import { Grid } from '@material-ui/core'
import { styled } from '@material-ui/core/styles'

export const AttributeContainer = styled(Grid)(({ theme }) => ({

  margin: theme.spacing(3, 'auto')

}))
