// eslint-disable-next-line no-use-before-define
import React, { useState } from 'react'
import { Grid, FormControl, TextField, Button } from '@material-ui/core'
import { AddBox, Save } from '@material-ui/icons'

import Container from '../container'
import AttributeGroup from './components/attributeGroup'
import Modal from 'components/modal'
import { useCategoriesState } from './context'
import { generateID } from 'services/hashService'
import usePostAPI from 'hooks/usePostAPI'
import { LinearProgress } from 'components/progress'
import Toast from 'components/toast'

function CreateCategory () {
  const [modalGroupOpen, setModalGroupOpen] = useState<boolean>(false)
  const [groupTitle, setGroupTitle] = useState<string>('')

  const { state, dispatch } = useCategoriesState()
  const [saveCategoryResponse, saveCategoryAPI] = usePostAPI({
    url: '/api/v1/admin/categories'
  })

  const { isLoading, success } = saveCategoryResponse

  const addAttributeGroup = (e:React.MouseEvent) => {
    if (groupTitle !== '') {
      dispatch({
        type: 'ADD_ATTRIBUTE_CATEGORY',
        payload: {
          hash: generateID(),
          title: groupTitle
        }
      })
      setModalGroupOpen(false)
    }
  }

  const addAttributeToGroup = (groupHash: string) => {
    dispatch({
      type: 'ADD_ATTRIBUTE',
      payload: {
        groupHash,
        attribute: {
          hash: generateID(),
          title: '',
          slug: '',
          filterable: false,
          hasPrice: false
        }
      }
    })
  }

  const updateAttribute = (attrHash:string, field:string, value: string | boolean) => {
    dispatch({
      type: 'UPDATE_ATTRIBUTE',
      payload: {
        attrHash,
        data: {
          [field]: value
        }
      }
    })
  }

  const updateCategoryTitle = (title: string) => {
    dispatch({
      type: 'UPDATE_CATEGORY_TITLE',
      payload: {
        title
      }
    })
  }

  const updateCategorySlug = (slug: string) => {
    dispatch({
      type: 'UPDATE_CATEGORY_SLUG',
      payload: {
        slug
      }
    })
  }

  const deleteAttributeGroup = (groupHash:string) => {
    dispatch({
      type: 'DELETE_ATTRIBUTE_CATEGORY',
      payload: {
        groupHash
      }
    })
  }

  const saveCategory = () => {
    saveCategoryAPI({
      ...state
    })
  }

  const closeModalGroup = (e:React.MouseEvent) => {
    setModalGroupOpen(false)
  }

  const showModalGroup = (e:React.MouseEvent) => {
    setModalGroupOpen(true)
  }

  const renderAttributeGroup = state.groups.map((group) => (
    <AttributeGroup
      key={group.hash}
      title={group.title}
      attributes={group.attributes}
      onAddAttribute={(e) => {
        addAttributeToGroup(group.hash)
      }}
      onUpdateAttribute={updateAttribute}
      onDeleteAttributeGroup={(e) => {
        deleteAttributeGroup(group.hash)
      }}
      />
  ))

  return (
        <Container title="ایجاد دسته بندی">
          {isLoading && <LinearProgress />}
          <Toast open={success} type="success" message="دسته بندی با موفقیت ایجاد شد" />
          <Grid container spacing={2}>
            <Grid item xs={12} md={6}>
              <FormControl fullWidth>
                <TextField
                variant="outlined"
                label="عنوان فارسی دسته بندی"
                id="title"
                name="title"
                defaultValue={state.title}
                onBlur={(e:React.FocusEvent<HTMLInputElement>) => { updateCategoryTitle(e.currentTarget.value) } }
                fullWidth />
              </FormControl>
            </Grid>
            <Grid item xs={12} md={6}>
              <FormControl fullWidth>
                <TextField
                variant="outlined"
                label="عنوان انگلیسی دسته بندی"
                id="slug"
                name="slug"
                defaultValue={state.slug}
                onBlur={(e:React.FocusEvent<HTMLInputElement>) => { updateCategorySlug(e.currentTarget.value) }}
                fullWidth />
              </FormControl>
            </Grid>
            <Grid item xs={12} md={12}>
              {renderAttributeGroup}
            </Grid>
            <Grid item xs={12} md={12}>
              <FormControl>
                <Button variant="contained" color="primary" startIcon={<AddBox />} onClick={showModalGroup}>اضافه کردن دسته بندی ویژگی ها</Button>
              </FormControl>
            </Grid>
            <Grid item xs={12} md={12} justify="center">
                <Button variant="contained" color="default" startIcon={<Save />} onClick={saveCategory}>ذخیره سازی</Button>
            </Grid>
            <Modal
              open={modalGroupOpen}
              title="عنوان دسته بندی خاصیت ها"
              onClose={closeModalGroup}
              actions={<>
                <Button variant="contained" color="secondary" onClick={closeModalGroup}>بستن</Button>
                <Button variant="contained" color="primary" onClick={addAttributeGroup}>تایید</Button>
              </>}
            >
              <TextField
                id="attribute_group_title"
                variant="outlined"
                label="عنوان"
                onChange={(e:React.ChangeEvent<HTMLInputElement>) => { setGroupTitle(e.target.value) }}
                fullWidth
              />
            </Modal>
          </Grid>
        </Container>
  )
}

export default CreateCategory
