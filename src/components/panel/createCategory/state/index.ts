import Action from 'contracts/Action'
import AttributeGroupType from '../components/attributeGroup/AttributeGroupType'

export interface CategoriesState {
  title: string;
  slug: string;
  groups: AttributeGroupType[];

}

export const initState: CategoriesState = {
  title: '',
  slug: '',
  groups: []
}

export const reducer = (state: CategoriesState, action: Action): CategoriesState => {
  let newState: CategoriesState

  switch (action.type) {
    case 'ADD_ATTRIBUTE':
      newState = {
        ...state,
        groups: state.groups.map((group) => {
          if (group.hash === action.payload.groupHash) {
            return {
              ...group,
              attributes: [
                ...group.attributes,
                {
                  ...action.payload.attribute
                }
              ]
            }
          }

          return group
        })
      }
      break
    case 'ADD_ATTRIBUTE_CATEGORY':
      newState = {
        ...state,
        groups: [
          ...state.groups,
          {
            hash: action.payload.hash,
            title: action.payload.title,
            attributes: []
          }
        ]
      }
      break
    case 'UPDATE_ATTRIBUTE':
      newState = {
        ...state,
        groups: state.groups.map((group) => {
          const newAttributes = group.attributes.map(attribute => {
            if (attribute.hash === action.payload.attrHash) {
              return {
                ...attribute,
                ...action.payload.data
              }
            }
            return attribute
          })

          group.attributes = newAttributes

          return group
        })
      }
      break
    case 'UPDATE_CATEGORY_TITLE':
      newState = {
        ...state,
        title: action.payload.title
      }
      break
    case 'UPDATE_CATEGORY_SLUG':
      newState = {
        ...state,
        slug: action.payload.slug
      }
      break
    case 'DELETE_ATTRIBUTE_CATEGORY':
      newState = {
        ...state,
        groups: state.groups.filter(group => group.hash !== action.payload.groupHash)
      }
      break
    default:
      throw new Error(`${action.type} is not defined!`)
  }

  return newState
}
