// eslint-disable-next-line no-use-before-define
import React from 'react'

import { CategoriesState, initState, reducer } from '../state'
import Action from 'contracts/Action'

interface CategoriesContextProps {

    state: CategoriesState;
    dispatch: React.Dispatch<Action>

}

const CategoriesContext = React.createContext<CategoriesContextProps>(
    {} as CategoriesContextProps
)

export const CategoriesProvider = ({ children }: React.PropsWithChildren<{}>) => {
  const [state, dispatch] = React.useReducer(reducer, initState)

  return (
        <CategoriesContext.Provider value={{ state, dispatch }}>
        { children }
        </CategoriesContext.Provider>
  )
}

export const useCategoriesState = () => {
  return React.useContext(CategoriesContext)
}
