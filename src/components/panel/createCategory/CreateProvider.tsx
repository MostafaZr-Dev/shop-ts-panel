// eslint-disable-next-line no-use-before-define
import React from 'react'

import { CategoriesProvider } from './context'
import CreateCategory from './CreateCategory'

function CreateProvider () {
  return (
        <CategoriesProvider>
            <CreateCategory />
        </CategoriesProvider>
  )
}

export default CreateProvider
