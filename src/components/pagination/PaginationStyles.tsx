import { Box } from '@material-ui/core'
import { Pagination } from '@material-ui/lab'
import { styled } from '@material-ui/core/styles'

export const PaginationContainer = styled(Box)(({ theme }) => ({
  display: 'flex',
  margin: theme.spacing(2, 'auto')
}))

export const StyledPagination = styled(Pagination)(({ theme }) => ({
  '& .MuiPaginationItem-page.Mui-selected': {
    backgroundColor: '#fb9300'
  }
}))
