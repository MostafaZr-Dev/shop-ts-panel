/* eslint-disable no-use-before-define */
import React from 'react'

import { PaginationContainer, StyledPagination } from './PaginationStyles'

type PaginationProps = {
    count:number;
    currentPage?:number | undefined;
    onChange?: ((e:React.ChangeEvent<unknown>, page:number) => void) | undefined
}

function CustomPagination ({ count, currentPage, onChange }:PaginationProps) {
  return (
    <PaginationContainer>
      <StyledPagination count={count} page={currentPage} onChange={onChange}/>
    </PaginationContainer>
  )
}

export default CustomPagination
