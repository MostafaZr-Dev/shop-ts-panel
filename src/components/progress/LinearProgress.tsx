// eslint-disable-next-line no-use-before-define
import React from 'react'
import { LinearProgress } from '@material-ui/core'

import { LinearProgressWrapper } from './ProgressStyles'
import CircularProgress from './CircularProgress'

type ProgressProps = {
  determinate?: boolean | undefined;
  value?: number | undefined;
}

function CustomLinearProgress ({ determinate, value }: ProgressProps) {
  return (
        <LinearProgressWrapper>
            {!determinate && <LinearProgress />}
            {determinate && <CircularProgress progress={value} />}
        </LinearProgressWrapper>
  )
}

export default CustomLinearProgress
