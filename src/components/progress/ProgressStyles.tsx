import { Box } from '@material-ui/core'
import { styled } from '@material-ui/core/styles'

export const LinearProgressWrapper = styled(Box)({

  display: 'flex',
  backgroundColor: 'rgba(51,51,51,0.44)',
  position: 'fixed',
  top: 0,
  right: 0,
  width: '100%',
  height: '100vh',
  zIndex: 999,

  '& .MuiLinearProgress-root': {
    width: '100%',

    '& .MuiLinearProgress-barColorPrimary': {
      backgroundColor: '#b6bce2'
    }

  },

  '& .MuiLinearProgress-colorPrimary': {
    backgroundColor: '#fb9300'
  }

})
