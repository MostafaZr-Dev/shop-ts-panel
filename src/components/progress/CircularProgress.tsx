// eslint-disable-next-line no-use-before-define
import React from 'react'
import CircularProgress, { CircularProgressProps } from '@material-ui/core/CircularProgress'
import { Typography, Box } from '@material-ui/core'
import { styled } from '@material-ui/core/styles'

const Container = styled(Box)({
  width: '100%',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center'
})

const StyledCircularProgress = styled(CircularProgress)({
  '& .MuiCircularProgress-colorPrimary': {
    color: '#fb9300'
  }
})

function CircularProgressWithLabel (props: CircularProgressProps & { value: number | undefined }) {
  return (
    <Box position="relative" display="inline-flex">
      <StyledCircularProgress variant="determinate" {...props} />
      <Box
        top={0}
        left={0}
        bottom={0}
        right={0}
        position="absolute"
        display="flex"
        alignItems="center"
        justifyContent="center"
      >
        <Typography variant="caption" component="div" color="textPrimary">{`${Math.round(
          props.value as number
        )}%`}</Typography>
      </Box>
    </Box>
  )
}

type CustomCircularProgressProps = {
    progress: number | undefined
}

export default function CustomCircularProgress ({ progress }:CustomCircularProgressProps) {
  return <Container>
    <CircularProgressWithLabel value={progress} />
  </Container>
}
