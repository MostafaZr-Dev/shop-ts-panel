// eslint-disable-next-line no-use-before-define
import React from 'react'
import {
  Paper,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Table
} from '@material-ui/core'

import { StyledHeader } from './TableStyles'

type TableProps = {
    column: string[];
    data: object[];
    attributes: string[];
}

const getProperty = <T extends object, U extends keyof T>(key:U) => (obj:T) => obj[key]

function CustomTable ({ column, data, attributes }:TableProps) {
  return (
    <TableContainer component={Paper}>
    <Table aria-label="simple table">
      <StyledHeader>
        <TableRow>
            {column.map((col, i) => (<TableCell key={i} align="center">{col}</TableCell>)
            )}
        </TableRow>
      </StyledHeader>
      <TableBody>
        {data.map((item, i) => (
            <TableRow key={i}>
              {attributes.map((attr, i) => (<TableCell align="center" key={i}>
                {getProperty(attr as never)(item)}
              </TableCell>))}
            </TableRow>
        ))}
      </TableBody>
    </Table>
  </TableContainer>
  )
}

export default CustomTable
