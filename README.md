#  Shop Project With Typescript
shop project with ** express - typescript - react - nextjs **

**front**
[front-repo](https://gitlab.com/MostafaZr-Dev/shop-ts-front)

**server**
[server-repo](https://gitlab.com/MostafaZr-Dev/shop-ts-server)

![enter image description here](https://s4.uupload.ir/files/shop-ts-panel_z9ez.jpg)

![enter image description here](https://s4.uupload.ir/files/shop-ts-panel2_k99l.jpg)
##  Technologies

  
### server
- Express
- Multer
- MongoDB(Mongoose)
- Redis
### admin-panel
- react
- react-router
- context-api
- material-ui
- axios
### front
- nextjs
- context-api
- clab-template
  

##  Features

- manage products ( create | update | delete )
- manage categories ( create-category with attributes )
- products list
- shopping cart 
- coupon
- payment ( zarinpal )
- notification ( sms - email )
- settings
